package com.wamsmobile.kepapaya.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.pojos.Dispatch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;

/**
 * Created by dell on 08/05/2017.
 */

public class ShoppingPayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_DISPATCH = 0;
    private static final int TYPE_LOAD = 1;

    private Context context;
    private List<Dispatch> dispatches;
    private OnLoadMoreListener loadMoreListener;
    private boolean isLoading = false, isMoreDataAvailable = true;

       /*
    * isLoading - to set the remote loading and complete status to fix back to back load more call
    * isMoreDataAvailable - to set whether more data from server available or not.
    * It will prevent useless load more request even after all the server data loaded
    * */


    public ShoppingPayAdapter(Context context, List<Dispatch> dispatches) {
        this.context = context;
        this.dispatches = dispatches;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_DISPATCH) {
            return new DispatchHolder(inflater.inflate
                    (R.layout.item_shopping_pay, parent, false));
        } else {
            return new LoadHolder(inflater.inflate
                    (R.layout.load_more_progressbar, parent, false));
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_DISPATCH) {
            ((DispatchHolder) holder).bindData(dispatches.get(position));
        }
        //No else part needed as load holder doesn't bind any data

    }

    @Override
    public int getItemViewType(int position) {
        return dispatches.get(position).listName.equals("Carrito de Compras")
                ? TYPE_DISPATCH : TYPE_LOAD;
    }

    @Override
    public int getItemCount() {
        return dispatches.size();
    }

    /* VIEW HOLDERS */

    private static class DispatchHolder extends RecyclerView.ViewHolder {
        TextView tvPaymentDate;
        TextView tvDeliveryDate;
        TextView tvAmountCharged;

        DispatchHolder(View itemView) {
            super(itemView);
            tvPaymentDate = itemView.findViewById(R.id.tv_payment_date_result);
            tvDeliveryDate = itemView.findViewById(R.id.tv_delivery_date_result);
            tvAmountCharged = itemView.findViewById(R.id.tv_amount_charged_result);
        }

        void bindData(Dispatch dispatch) {
            tvPaymentDate.setText(getFormattedDateFromTimestamp(dispatch.payedDate));
            tvDeliveryDate.setText(getFormattedDateFromTimestamp(dispatch.deliveryDate));
            double amountCharged = decimalsRounder(dispatch
                    .transactions.get(0).getAmount(), 2);
            String mAmountCharged = Double.toString(amountCharged);
            tvAmountCharged.setText("$" + mAmountCharged);
        }
    }

    private static class LoadHolder extends RecyclerView.ViewHolder {
        LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
        call adapter.notifyDataChanged(); after update the list
        */
    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    // Method to convert to a date format
    private static String getFormattedDateFromTimestamp(long timestampInMilliSeconds) {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String formattedDate = new SimpleDateFormat("MMM d, yyyy").format(date);
        return formattedDate;
    }
}
