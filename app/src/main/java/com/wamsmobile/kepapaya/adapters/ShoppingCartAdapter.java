package com.wamsmobile.kepapaya.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.fragments.main.FragmentDialogDiscard;
import com.wamsmobile.kepapaya.fragments.main.FragmentDialogShoppingcartAdd;
import com.wamsmobile.kepapaya.holders.ShoppingCartHolder;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.pojos.ItemCartModel;
import com.wamsmobile.kepapaya.utilities.CustomTypefaceSpan;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;


/**
 * Created by root on 2/3/16.
 */
public class ShoppingCartAdapter extends CollectionAdapter<ItemCartModel, ShoppingCartHolder> {

    private Context context;
    private List<ItemCartModel> dataSource;
    private int positionThis;

    /**
     * Default constructor
     *
     * @param context    android Context
     * @param dataSource List<T> Collection to render
     * @param layoutId   int layout resource id to inflate
     */
    public ShoppingCartAdapter(Context context, List<ItemCartModel> dataSource, int layoutId) {
        super(context, dataSource, layoutId);
        this.context = context;
        this.dataSource = dataSource;
    }


    @Override
    protected ShoppingCartHolder newHolderInstance(View rowView) {
        return new ShoppingCartHolder(rowView);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    @Override
    protected void bindHolder(ShoppingCartHolder holder, final ItemCartModel item, final int position) {
        //String price = Double.toString(item.getItem().getPrice());
        //price = "$" + price;

        String name = item.getItem().getName();
        String quantity = "x" + Integer.toString(item.getQuantity());
        Double price = item.getItem().getPrice();
        String total = Double.toString(decimalsRounder(price * (item.getQuantity()), 2));
        total = "$" + total;

        String nameQuantity = name + " " + quantity;
        holder.tvNameQuantity.setText(getEditedString(nameQuantity, name.length()));
        holder.subTotal.setText(total);

        setOnItemClickListener(new CollectionAdapter.OnItemClickListener<ItemCartModel, ShoppingCartHolder>() {
            @Override
            public void onItemClick(ItemCartModel item, ShoppingCartHolder holder, int position) {
                showDialogShoppingcart(item);
            }
        });

        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentDialogDiscard customDialogFragment = new FragmentDialogDiscard();
                FragmentManager supportFragmentManager =
                        ((AppCompatActivity) context).getSupportFragmentManager();
                customDialogFragment.setCustomOnClickListener(new FragmentDialogDiscard.CustomOnClickListener() {
                    @Override
                    public void onClick() {
                        // continue with discard
                        deleteItemRequest(position);
                        positionThis = position;
                    }
                });
                customDialogFragment.show(supportFragmentManager, FragmentDialogDiscard.ARG_ITEM_ID);
            }
        });
    }

    // Pass info and open fragment
    private void showDialogShoppingcart(ItemCartModel item) {
        FragmentDialogShoppingcartAdd customDialogFragment = new FragmentDialogShoppingcartAdd();
        Bundle bundle = new Bundle();
        bundle.putString("ID", item.getItem().getId());
        bundle.putString("NAME", item.getItem().getName());
        bundle.putString("IMAGE", item.getItem().getImages().get(0));
        bundle.putString("BRAND", item.getItem().getBrandName());
        bundle.putInt("QUANTITY", item.getQuantity());
        bundle.putString("PRICE", "" + item.getItem().getPrice());
        bundle.putString("CATEGORY", item.getItem().getCategoryName());
        bundle.putString("MEASSURE", item.getItem().getMeassure());
        bundle.putString("FRAGMENT", "isChild");

        customDialogFragment.setArguments(bundle);

        customDialogFragment.show(((MainActivity) context).getSupportFragmentManager(),
                FragmentDialogShoppingcartAdd.ARG_ITEM_ID);
    }

    private void deleteItemRequest(int pos) {
        Toast.makeText(context, R.string.product_discarded, Toast.LENGTH_SHORT).show();
        String id = getItem(pos).getItem().getId();
        int quantity = getItem(pos).getQuantity();
        LocalStorage localStorage = new LocalStorage(context);
        String token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        String tokenSecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        Call<EMesResponse> call = RestClient.buildService().deleteItemFromList(id, token, tokenSecret, quantity);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        EMesResponse deleteResponse = response.body();
                        delete(positionThis);
                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.body().getMes();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private SpannableStringBuilder getEditedString(String string, int firstLength) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);

        Typeface avenirRoman = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir-Roman.otf");
        Typeface avenirHeavy = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir-Heavy.otf");

        TypefaceSpan avenirRomanSpan = new CustomTypefaceSpan("", avenirRoman);
        TypefaceSpan avenirHeavySpan = new CustomTypefaceSpan("", avenirHeavy);

        // roman font for 1sts chars
        spannableStringBuilder.setSpan(avenirRomanSpan, 0, firstLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // heavy font for rest of the chars
        spannableStringBuilder.setSpan(avenirHeavySpan, firstLength, string.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // also change color for rest of the chars
        spannableStringBuilder.setSpan
                (new ForegroundColorSpan(ResourcesCompat.getColor(context.getResources(), R.color.purple, null)),
                        firstLength, string.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // 1.125f is 18/16sp that is the size of the first string
        spannableStringBuilder.setSpan(new RelativeSizeSpan(1.125f), firstLength, string.length(), 0); // set size
        return spannableStringBuilder;
    }
}