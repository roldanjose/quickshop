package com.wamsmobile.kepapaya.adapters;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.RequestManager;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.application.KepapayaApplication;
import com.wamsmobile.kepapaya.fragments.main.FragmentDialogShoppingcartAdd;
import com.wamsmobile.kepapaya.holders.ProductHolder;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.TokensUser;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.responseDataModel.Product;
import com.wamsmobile.kepapaya.utilities.OnSwipeTouchListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 03/04/2018.
 */

public class ProductsAdapter extends CollectionAdapter<Product, ProductHolder> {

    private Context context;
    private List<Product> dataSource;
    private RequestManager glide;
    private String token, tokenSecret, productID;
    private TokensUser tokensUser;
    private ObjectAnimator linearAnimator;
    private ObjectAnimator buttonAnimator;

    /**
     * Default constructor
     *
     * @param context    android Context
     * @param dataSource List<T> Collection to render
     * @param layoutId   int layout resource id to inflate
     */
    public ProductsAdapter(Context context, List<Product> dataSource,
                           int layoutId, RequestManager glide) {
        super(context, dataSource, layoutId);
        this.context = context;
        this.dataSource = dataSource;
        this.glide = glide;

        LocalStorage localStorage = new LocalStorage(context);
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokenSecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);
        tokensUser = new TokensUser(token, tokenSecret);
    }

    @Override
    protected ProductHolder newHolderInstance(View rowView) {
        return new ProductHolder(rowView);
    }

    @Override
    protected void bindHolder(final ProductHolder holder, final Product product, int position) {
        productID = product.getId();
        String name = product.getName();
        String url = product.getImage().getUrl();
        //String size = String.valueOf(product.ge);
        String measure = product.getMeasure();
        String price = product.getPrice();
        //TODO: product.isfavorite? from backend
       boolean isFavorite = product.isFavorite();
        String description = product.getDescriptionLong();

        holder.tvProductName.setText(name);
        holder.tvSizeMeasure.setText(measure);
        holder.tvPrice.setText(price + "Bs");
        holder.tvDescription.setText(description);

        //TODO:
        boolean isAvailable = (product.getStock() >= 1);
        if (isAvailable) {
            holder.tvAvailability.setTextColor(ContextCompat.getColor(context, R.color.green1));
            holder.imgAvailabilityIcon.setImageResource(R.drawable.ic_fill);
        } else {
            holder.tvAvailability.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.imgAvailabilityIcon.setImageResource(R.drawable.ic_red);
        }

        glide.load(url)
                .into(holder.imgProduct);

        if (isFavorite) {
            holder.heartFavoriteBtn.setImageResource(R.drawable.ic_hn);
        }
        holder.heartFavoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!product.isFavorite()) {
                    holder.heartFavoriteBtn.setClickable(false);
                    holder.heartFavoriteBtn.setImageResource(R.drawable.ic_hn);
                    //addFavoriteCall(holder, product);
                }
                if (product.isFavorite()) {
                    holder.heartFavoriteBtn.setClickable(false);
                    holder.heartFavoriteBtn.setImageResource(R.drawable.ic_hb);
                    //deleteFavoriteCall(holder, product);
                }
            }
        });
        holder.carShopPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentDialogShoppingcartAdd customDialogFragment = new FragmentDialogShoppingcartAdd();
                Bundle bundle = new Bundle();
                bundle.putString("ID", productID);
                bundle.putString("NAME", product.getName());
                bundle.putString("IMAGE", product.getImage().getUrl());
                bundle.putString("BRAND", product.getBrand());
                bundle.putString("PRICE", product.getPrice());
                bundle.putString("CATEGORY", product.getCategory());
                bundle.putString("MEASSURE", product.getMeasure());
                bundle.putString("FRAGMENT", "isSearch");
                customDialogFragment.setArguments(bundle);

                customDialogFragment.show(((MainActivity) context).getSupportFragmentManager(),
                        FragmentDialogShoppingcartAdd.ARG_ITEM_ID);
            }
        });
        holder.btnShowInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateView(holder, product);
            }
        });
        holder.linearInfo.setOnTouchListener(new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeLeft() {
                if (!product.isOpen()) {
                    showInfo(holder, product);
                }
            }

            @Override
            public void onSwipeRight() {
                if (product.isOpen()) {
                    holder.tvAvailability.animate().alpha(0.0f);
                    hideInfo(holder, product);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    @Override
    public void onViewAttachedToWindow(ProductHolder holder) {
        super.onViewAttachedToWindow(holder);
        positioningComponents(holder);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ProductHolder productHolder = super.onCreateViewHolder(parent, viewType);
        positioningComponents(productHolder);
        return productHolder;

    }

    private void positioningComponents(ProductHolder holder) {
        int linearWidth = getScreenPxWidth() - dpToPx(46);
        RelativeLayout.LayoutParams linearParams = new RelativeLayout.LayoutParams(linearWidth,
                LinearLayout.LayoutParams.WRAP_CONTENT); // or set height to any fixed value you want
        linearParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        holder.linearInfo.setLayoutParams(linearParams);

        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) holder.linearInfo.getLayoutParams();
        int percentWidth = (int) getPercentWidth(holder);
        params.rightMargin = -percentWidth;
        params.setMarginEnd(-percentWidth);

        RelativeLayout.LayoutParams buttonParams = new RelativeLayout.LayoutParams(dpToPx(50), dpToPx(50)); // or set height to any fixed value you want
        buttonParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        buttonParams.addRule(RelativeLayout.CENTER_VERTICAL);
        holder.btnShowInfo.setLayoutParams(buttonParams);

        ViewGroup.MarginLayoutParams params2 =
                (ViewGroup.MarginLayoutParams) holder.btnShowInfo.getLayoutParams();
        int buttonMargin = linearWidth - percentWidth - dpToPx(25);
        params2.rightMargin = buttonMargin;
        params2.setMarginEnd(buttonMargin);
    }

    private int getScreenPxWidth() {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point.x;
    }

    private void animateView(ProductHolder holder, Product product) {
        holder.btnShowInfo.setClickable(false);
        if (product.isOpen()) {
            holder.tvAvailability.animate().alpha(0.0f);
            hideInfo(holder, product);
        } else {
            showInfo(holder, product);
        }
    }

    private float getPercentWidth(ProductHolder holder) {
        float width = holder.linearInfo.getWidth();
        return (float) (width * 0.45);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void showInfo(final ProductHolder holder, final Product product) {
        float percentWidth = -getPercentWidth(holder);
        linearAnimator = ObjectAnimator.ofFloat(holder.linearInfo, "translationX", 0, percentWidth);
        linearAnimator.start();

        buttonAnimator = ObjectAnimator.ofFloat(holder.btnShowInfo, "translationX", 0, percentWidth);
        buttonAnimator.start();

        linearAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                holder.btnShowInfo.setImageResource(R.drawable.bt_dspl);
                product.setOpen(true);
                holder.btnShowInfo.setClickable(true);
                holder.tvAvailability.animate().alpha(1.0f);
                holder.tvAvailability.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        /*Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_right_to_center);
        holder.linearInfo.startAnimation(animation);
        holder.btnShowInfo.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                holder.btnShowInfo.setImageResource(R.drawable.bt_dspl);
                isOpen = true;
                holder.btnShowInfo.setClickable(true);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.linearInfo.getLayoutParams();
                layoutParams.rightMargin = getPercentWidth(holder);
                layoutParams.leftMargin = layoutParams.getMarginStart() - getPercentWidth(holder);
                holder.linearInfo.setLayoutParams(layoutParams);
                layoutParams =  (RelativeLayout.LayoutParams) holder.btnShowInfo.getLayoutParams();
                layoutParams.leftMargin = layoutParams.getMarginStart() - getPercentWidth(holder);
                holder.btnShowInfo.setLayoutParams(layoutParams);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/
    }

    private void hideInfo(final ProductHolder holder, final Product product) {
        float percentWidth = -getPercentWidth(holder);
        linearAnimator = ObjectAnimator.ofFloat(holder.linearInfo, "translationX", percentWidth, 0);
        linearAnimator.start();

        buttonAnimator = ObjectAnimator.ofFloat(holder.btnShowInfo, "translationX", percentWidth, 0);
        buttonAnimator.start();

        linearAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                holder.btnShowInfo.setImageResource(R.drawable.ic_mo);
                product.setOpen(false);
                holder.btnShowInfo.setClickable(true);
                holder.tvAvailability.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        /*Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_center_to_right);
        holder.linearInfo.startAnimation(animation);
        holder.btnShowInfo.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                holder.btnShowInfo.setImageResource(R.drawable.ic_mo);
                isOpen = false;
                holder.btnShowInfo.setClickable(true);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.linearInfo.getLayoutParams();
                layoutParams.rightMargin = - getPercentWidth(holder);
                layoutParams.leftMargin = layoutParams.getMarginStart() + getPercentWidth(holder);
                holder.linearInfo.setLayoutParams(layoutParams);
                layoutParams =  (RelativeLayout.LayoutParams) holder.btnShowInfo.getLayoutParams();
                layoutParams.leftMargin = layoutParams.getMarginStart() + getPercentWidth(holder);
                holder.btnShowInfo.setLayoutParams(layoutParams);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/
    }

    private void addFavoriteCall(final ProductHolder holder, final Product product) {
        Call<EMesResponse> call = RestClient.buildService().addToFavorites(productID, tokensUser);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        holder.heartFavoriteBtn.setClickable(true);
                        EMesResponse listResponse = response.body();
                        product.setFavorite(true);
                        KepapayaApplication.showToast((MainActivity) context, R.string.added_to_favorites, Toast.LENGTH_LONG);
                        Toast.makeText(context, R.string.added_to_favorites, Toast.LENGTH_LONG).show();
                    } else {
                        holder.heartFavoriteBtn.setClickable(true);
                        String error = response.body().getMes();
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    holder.heartFavoriteBtn.setClickable(true);
                    String error = response.body().getMes();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                holder.heartFavoriteBtn.setClickable(true);
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void deleteFavoriteCall(final ProductHolder holder, final Product product) {
        Call<EMesResponse> call = RestClient.buildService().deleteFromFavorites(productID, token, tokenSecret);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        holder.heartFavoriteBtn.setClickable(true);
                        EMesResponse listResponse = response.body();
                        product.setFavorite(false);
                        Toast.makeText(context, R.string.removed_from_favorites, Toast.LENGTH_LONG).show();

                    } else {
                        holder.heartFavoriteBtn.setClickable(true);
                        String error = response.body().getMes();
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    holder.heartFavoriteBtn.setClickable(true);
                    String error = response.body().getMes();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                holder.heartFavoriteBtn.setClickable(true);
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
