package com.wamsmobile.kepapaya.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.holders.AffiliatedAddressesHolder;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.RestClient360;
import com.wamsmobile.kepapaya.pojos.Address;
import com.wamsmobile.kepapaya.pojos.EMesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 10/04/2017.
 */

public class AffiliatedAdressesAdapter extends CollectionAdapter<Address,AffiliatedAddressesHolder> {

    private String token = "vkl1DQW84";
    private String tokenSecret = "579e4e3d-070c-49f5-972d-493ce814c96b";
    private Context context;
    private List<Address> dataSource;

    /**
     * Default constructor
     *
     * @param context    android Context
     * @param dataSource List<T> Collection to render
     * @param layoutId   int layout resource id to inflate
     */
    public AffiliatedAdressesAdapter(Context context, List<Address> dataSource, int layoutId) {
        super(context, dataSource, layoutId);
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    @Override
    protected AffiliatedAddressesHolder newHolderInstance(View rowView) {
        return new AffiliatedAddressesHolder(rowView);
    }

    @Override
    protected void bindHolder(AffiliatedAddressesHolder holder, final Address item, final int position) {
        String alias = item.getAlias();
        String address = item.getAddress();
        String contact = item.getContact();
        String colony = item.getColony();

        holder.tvAlias.setText(alias);
        holder.tvAddress.setText(address);
        holder.tvColony.setText(colony);
        holder.tvContact.setText(contact);

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(position);
            }
        });
    }

    private void deleteRequest(final int position) {
        String addressID = dataSource.get(position).getId();
        Call<EMesResponse> call = RestClient360.buildService().deleteUserAddress360(addressID, token, tokenSecret);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        Intent intentRefresh = new Intent(KeyStore.REFRESH_ADDRESSES);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intentRefresh);
                        Toast.makeText(context, R.string.address_deleted, Toast.LENGTH_SHORT).show();
                        delete(position);

                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.body().getMes();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showDeleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.delete)
                .setMessage(R.string.delete_this_address)
                .setPositiveButton(R.string.dialog_accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        deleteRequest(position);
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
