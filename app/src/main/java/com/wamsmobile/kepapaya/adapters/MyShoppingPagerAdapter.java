package com.wamsmobile.kepapaya.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.fragments.main.FragmentViewPager;

/**
 * Created by WAMS 5 on 20/03/2017.
 */

public class MyShoppingPagerAdapter extends FragmentPagerAdapter {

    private int tabTitles[] = {R.string.shopping_paid,
            R.string.shopping_sent, R.string.shopping_delivered};

    private Activity activity;

    public MyShoppingPagerAdapter(FragmentManager fm, Activity a) {
        super(fm);
        this.activity = a;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = FragmentViewPager.newInstance(5, 0, 1);
                break;
            case 1:
                fragment = FragmentViewPager.newInstance(5, 0, 2);
                break;
            case 2:
                fragment = FragmentViewPager.newInstance(5, 0, 3);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return activity.getResources().getString(tabTitles[position]);
    }
}
