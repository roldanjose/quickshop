package com.wamsmobile.kepapaya.adapters;

import android.content.Context;
import android.view.View;

import com.bumptech.glide.Glide;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.holders.CategoriesHolder;
import com.wamsmobile.kepapaya.responseDataModel.Category;

import java.util.List;

/**
 * Created by dell on 14/02/2018.
 */

public class CategoriesAdapter extends CollectionAdapter<Category, CategoriesHolder> {

    private Context context;
    private List<Category> dataSource;

    /**
     * Default constructor
     *
     * @param context    android Context
     * @param dataSource List<T> Collection to render
     * @param layoutId   int layout resource id to inflate
     */
    public CategoriesAdapter(Context context, List<Category> dataSource, int layoutId) {
        super(context, dataSource, layoutId);
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    @Override
    protected CategoriesHolder newHolderInstance(View rowView) {
        return new CategoriesHolder(rowView);
    }

    @Override
    protected void bindHolder(CategoriesHolder holder, Category item, int position) {
        String name = item.getCategoryName();
        String imageString = item.getImage();

        holder.tvCategory.setText(name);
        Glide.with(context)
                .load(imageString)
                .placeholder(R.drawable.placeholder)
                .into(holder.imgCategory);
    }
}
