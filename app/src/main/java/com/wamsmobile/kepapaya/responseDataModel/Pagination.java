package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

public class Pagination{

	@SerializedName("itemsPerPage")
	private int itemsPerPage;

	@SerializedName("totalPages")
	private int totalPages;

	@SerializedName("itemsCount")
	private int itemsCount;

	public void setItemsPerPage(int itemsPerPage){
		this.itemsPerPage = itemsPerPage;
	}

	public int getItemsPerPage(){
		return itemsPerPage;
	}

	public void setTotalPages(int totalPages){
		this.totalPages = totalPages;
	}

	public int getTotalPages(){
		return totalPages;
	}

	public void setItemsCount(int itemsCount){
		this.itemsCount = itemsCount;
	}

	public int getItemsCount(){
		return itemsCount;
	}
}