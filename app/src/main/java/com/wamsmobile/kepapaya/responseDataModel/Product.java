package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

public class Product {

	@SerializedName("image")
	private Image image;

	@SerializedName("descriptionLong")
	private String descriptionLong;

	@SerializedName("active")
	private String active;

	@SerializedName("tax")
	private double tax;

	@SerializedName("descriptionShort")
	private String descriptionShort;

	@SerializedName("createdAt")
	private int createdAt;

	@SerializedName("measure")
	private String measure;

	@SerializedName("erased")
	private boolean erased;

	@SerializedName("price")
	private String price;

	@SerializedName("__v")
	private int V;

	@SerializedName("name")
	private String name;

	@SerializedName("_id")
	private String id;

	@SerializedName("category")
	private String category;

	@SerializedName("stock")
	private int stock;

	@SerializedName("brand")
	private String brand;

    public void setImage(Image image){
		this.image = image;
	}

	public Image getImage(){
		return image;
	}

	public void setDescriptionLong(String descriptionLong){
		this.descriptionLong = descriptionLong;
	}

	public String getDescriptionLong(){
		return descriptionLong;
	}

	public void setActive(String active){
		this.active = active;
	}

	public String getActive(){
		return active;
	}

	public void setTax(double tax){
		this.tax = tax;
	}

	public double getTax(){
		return tax;
	}

	public void setDescriptionShort(String descriptionShort){
		this.descriptionShort = descriptionShort;
	}

	public String getDescriptionShort(){
		return descriptionShort;
	}

	public void setCreatedAt(int createdAt){
		this.createdAt = createdAt;
	}

	public int getCreatedAt(){
		return createdAt;
	}

	public void setMeasure(String measure){
		this.measure = measure;
	}

	public String getMeasure(){
		return measure;
	}

	public void setErased(boolean erased){
		this.erased = erased;
	}

	public boolean isErased(){
		return erased;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setStock(int stock){
		this.stock = stock;
	}

	public int getStock(){
		return stock;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

    private boolean isFavorite = false;

    private boolean isOpen = false;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}