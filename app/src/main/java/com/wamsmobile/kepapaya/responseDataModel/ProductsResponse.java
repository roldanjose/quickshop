package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsResponse {

	@SerializedName("pagination")
	private Pagination pagination;

	@SerializedName("items")
	private List<Product> items;

	public void setPagination(Pagination pagination){
		this.pagination = pagination;
	}

	public Pagination getPagination(){
		return pagination;
	}

	public void setItems(List<Product> items){
		this.items = items;
	}

	public List<Product> getItems(){
		return items;
	}
}