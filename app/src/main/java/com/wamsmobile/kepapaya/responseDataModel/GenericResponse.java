package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

public class GenericResponse <T>{

	@SerializedName("e")
	private String e;

	@SerializedName("mes")
	private T data;

	public void setE(int e){
		this.e = Integer.toString(e);
	}

	public int getE(){
		return Integer.valueOf(e);
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}