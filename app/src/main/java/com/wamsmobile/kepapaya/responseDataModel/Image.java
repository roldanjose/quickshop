package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

public class Image{

	@SerializedName("extension")
	private String extension;

	@SerializedName("url")
	private String url;

	public void setExtension(String extension){
		this.extension = extension;
	}

	public String getExtension(){
		return extension;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}
}