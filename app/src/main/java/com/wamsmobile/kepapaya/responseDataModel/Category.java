package com.wamsmobile.kepapaya.responseDataModel;

import com.google.gson.annotations.SerializedName;

public class Category {

	@SerializedName("erased")
	private boolean erased;

	@SerializedName("imgExtension")
	private String imgExtension;

	@SerializedName("__v")
	private int V;

	@SerializedName("active")
	private boolean active;

	@SerializedName("_id")
	private String id;

	@SerializedName("priority")
	private int priority;

	@SerializedName("categoryName")
	private String categoryName;

	@SerializedName("url")
	private String image;

	public void setErased(boolean erased){
		this.erased = erased;
	}

	public boolean isErased(){
		return erased;
	}

	public void setImgExtension(String imgExtension){
		this.imgExtension = imgExtension;
	}

	public String getImgExtension(){
		return imgExtension;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setActive(boolean active){
		this.active = active;
	}

	public boolean isActive(){
		return active;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPriority(int priority){
		this.priority = priority;
	}

	public int getPriority(){
		return priority;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}
}