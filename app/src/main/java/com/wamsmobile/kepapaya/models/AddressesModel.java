
package com.wamsmobile.kepapaya.models;

import java.util.List;

import com.wamsmobile.kepapaya.pojos.Address;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressesModel {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

}
