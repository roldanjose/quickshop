package com.wamsmobile.kepapaya.models;

/**
 * Created by dell on 27/02/2017.
 */

public class Login {

    private String mail;
    private String password;

    public Login(String mail, String password) {
        this.mail = mail;
        this.password = password;
    }

    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
