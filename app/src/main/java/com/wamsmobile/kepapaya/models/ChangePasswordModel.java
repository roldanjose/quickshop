package com.wamsmobile.kepapaya.models;

/**
 * Created by WAMS 5 on 20/03/2017.
 */

public class ChangePasswordModel {

    private String oldpassword;
    private String newpassword;
    private String token;
    private String tokensecret;

    public ChangePasswordModel(String oldpassword, String newpassword, String token, String tokensecret) {
        this.oldpassword = oldpassword;
        this.newpassword = newpassword;
        this.token = token;
        this.tokensecret = tokensecret;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokensecret() {
        return tokensecret;
    }

    public void setTokensecret(String tokensecret) {
        this.tokensecret = tokensecret;
    }
}
