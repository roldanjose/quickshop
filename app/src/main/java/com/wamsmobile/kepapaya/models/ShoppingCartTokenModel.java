package com.wamsmobile.kepapaya.models;

/**
 * Created by dell on 27/02/2017.
 */

public class ShoppingCartTokenModel {

    private String token;
    private String tokensecret;
    private int quantity;

    public ShoppingCartTokenModel(String token, String tokensecret, int quantity) {
        this.token = token;
        this.tokensecret = tokensecret;
        this.quantity = quantity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokensecret() {
        return tokensecret;
    }

    public void setTokensecret(String tokensecret) {
        this.tokensecret = tokensecret;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
