package com.wamsmobile.kepapaya.models;

/**
 * Created by dell on 27/02/2017.
 */

public class TokensUser {

    private String token;
    private String tokensecret;

    public TokensUser(String token, String tokensecret) {
        this.token = token;
        this.tokensecret = tokensecret;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokensecret() {
        return tokensecret;
    }

    public void setTokensecret(String tokensecret) {
        this.tokensecret = tokensecret;
    }
}
