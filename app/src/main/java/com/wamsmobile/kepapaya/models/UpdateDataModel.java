package com.wamsmobile.kepapaya.models;

/**
 * Created by WAMS 5 on 17/03/2017.
 */

public class UpdateDataModel {

    private String token;
    private String tokensecret;
    private String name;
    private String lastname;
    private String rfc;
    private String postal;
    private String dob;


    public UpdateDataModel(String token, String tokensecret, String name, String lastname, String rfc, String postal, String dob) {
        this.token = token;
        this.tokensecret = tokensecret;
        this.name = name;
        this.lastname = lastname;
        this.rfc = rfc;
        this.postal = postal;
        this.dob = dob;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokensecret() {
        return tokensecret;
    }

    public void setTokensecret(String tokensecret) {
        this.tokensecret = tokensecret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

}
