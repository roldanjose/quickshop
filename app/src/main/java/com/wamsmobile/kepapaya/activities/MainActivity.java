package com.wamsmobile.kepapaya.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.application.KepapayaApplication;
import com.wamsmobile.kepapaya.application.UserSingleton;
import com.wamsmobile.kepapaya.fragments.main.FragmentAffiliatedAddresses;
import com.wamsmobile.kepapaya.fragments.main.FragmentCategories;
import com.wamsmobile.kepapaya.fragments.main.FragmentDialogLoginMessage;
import com.wamsmobile.kepapaya.fragments.main.FragmentFindOrderId;
import com.wamsmobile.kepapaya.fragments.main.FragmentMyProfile;
import com.wamsmobile.kepapaya.fragments.main.FragmentPasswordChange;
import com.wamsmobile.kepapaya.fragments.main.FragmentShoppingCart;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.pojos.UserResponse;
import com.wamsmobile.kepapaya.utilities.CustomTypefaceSpan;
import com.wamsmobile.kepapaya.utilities.Label;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private View statubar;
    private Toolbar toolbar;
    private Label toolbarTitle, tvUserName, tvHello;
    private ImageButton toolbarFirstIndicator, toolbarLastIndicator;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private String token, tokensecret;
    private LocalStorage localStorage;

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }

    public void setToolbarTitle(int titleResId) {
        setToolbarTitle(getString(titleResId));
    }

    public void setToolbarTitleColor(String color) {
        int colorInt = Color.parseColor(color);
        setToolbarTitleColor(colorInt);
    }

    public void setToolbarTitleColor(int colorHex) {
        toolbarTitle.setTextColor(colorHex);
    }

    public void setToolbarTitleColorById(int colorResId) {
        int color = ContextCompat.getColor(this, colorResId);
        setToolbarTitleColor(color);
    }

    public void setToolbarFirstIndicator(int imgResId) {
        toolbarFirstIndicator.setImageResource(imgResId);
    }

    public ImageButton getToolbarFirstIndicator() {
        return toolbarFirstIndicator;
    }

    public void setToolbarLastIndicator(int imgResId) {
        toolbarLastIndicator.setImageResource(imgResId);
    }

    public void setToolbarFirstIndicatorListener(View.OnClickListener onClickListener) {
        toolbarFirstIndicator.setOnClickListener(onClickListener);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setListeners();
        // Open fragment of the menu
        initFragment(new FragmentCategories());
    }

    @Override
    protected void onResume() {
        super.onResume();
        configNavigationView();
    }

    private void initComponents() {
        statubar = findViewById(R.id.status_bar);
        updateStatusBarColor(R.color.blackTransparent);

        // Initiate toolbar elements
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarFirstIndicator = findViewById(R.id.btn_toolbar_first);
        toolbarLastIndicator = findViewById(R.id.btn_toolbar_last);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        } catch (NullPointerException npe) {
            Log.v("", "", npe);
        }

        // Initiate Navigation Drawer elements
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvUserName = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        tvHello = navigationView.getHeaderView(0).findViewById(R.id.user_gretting);

        localStorage = new LocalStorage(MainActivity.this);
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokensecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);
        setNavigationViewTypeface();
    }

    private void setListeners() {
        toolbarFirstIndicator.setOnClickListener(this);
        toolbarLastIndicator.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void updateStatusBarColor(int colorResID) {// Color resource ID
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, colorResID));
        }
        statubar.setBackgroundColor(ContextCompat.getColor(this, colorResID));
    }

    public void selectDrawerItem(int id) {
        navigationView.getMenu().findItem(id).setChecked(true);
    }

    private void configNavigationView() {
        if (isLoggedIn()) {
            // Metodo que obtiene los datos del usuario y coloca su nombre en el header
            getUserInfo();
            navigationView.getMenu().findItem(R.id.login_item).setVisible(false);
            navigationView.getMenu().findItem(R.id.logout_item).setVisible(true);
        } else {
            navigationView.getMenu().findItem(R.id.login_item).setVisible(true);
            navigationView.getMenu().findItem(R.id.logout_item).setVisible(false);
        }
    }

    public void getUserInfo() {
        Call<UserResponse> call = RestClient.buildService().getUserInfo(token, tokensecret);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        UserSingleton.get().setUserResponse(response.body());
                        // Get user name
                        UserResponse userResponse = UserSingleton.get().getUserResponse();
                        String userName = userResponse.getUser().getName();
                        tvUserName.setText(" " + userName);
                        tvUserName.setVisibility(View.VISIBLE);
                        tvHello.setVisibility(View.VISIBLE);
                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void applyFontToMenuItem(MenuItem menuItem) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Avenir-Book.otf");
        SpannableString mNewTitle = new SpannableString(menuItem.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        menuItem.setTitle(mNewTitle);
    }

    private void setNavigationViewTypeface() {
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem menuItem = m.getItem(i);

            //the method we have create in activity
            applyFontToMenuItem(menuItem);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_toolbar_first:
                manageDrawerLayout();
                break;
            case R.id.btn_toolbar_last:
                if (isLoggedIn()) {
                    initFragment(new FragmentShoppingCart());
                } else {
                    FragmentDialogLoginMessage customDialogFragment = new FragmentDialogLoginMessage();
                    customDialogFragment.show((this).getSupportFragmentManager(),
                            FragmentDialogLoginMessage.ARG_ITEM_ID);
                }
                break;
        }
    }

    public void manageDrawerLayout() {
        // Open or Close Navigation Drawer
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);

        } else {
            drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.login_item: {
                // Start authentication activity
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                startActivity(new Intent(MainActivity.this, AuthenticationActivity.class));
                break;
            }
            case R.id.categories_item: {
                initFragment(new FragmentCategories());
                break;
            }
            case R.id.shopping_cart_item: {
                initFragment(new FragmentShoppingCart());
                break;
            }
            case R.id.affiliated_addresses_item: {
                initFragment(new FragmentAffiliatedAddresses());
                break;
            }
            case R.id.payment_methods_item: {


                break;
            }
            case R.id.billing_item: {
                initFragment(new FragmentFindOrderId());
                break;
            }
            case R.id.my_profile_item: {
                initFragment(new FragmentMyProfile());
                break;
            }
            case R.id.change_password_item: {
                initFragment(new FragmentPasswordChange());
                break;
            }
            case R.id.shopping_history_item: {
                initFragment(new FragmentPasswordChange());
                break;
            }
            case R.id.my_shoppping_item: {
                initFragment(new FragmentPasswordChange());
                break;
            }
            case R.id.logout_item: {
                drawerLayout.closeDrawer(Gravity.START);
                logout();
                initFragment(new FragmentCategories());
                break;
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isLoggedIn() {
        return localStorage.getPermanentVariable(KeyStore.TOKEN) != null
                && !localStorage.getPermanentVariable(KeyStore.TOKEN).isEmpty();
    }

    public void logout() {
        navigationView.getMenu().findItem(R.id.login_item).setVisible(true);
        navigationView.getMenu().findItem(R.id.logout_item).setVisible(false);

        localStorage.setPermanentVariable(KeyStore.TOKEN, null);
        localStorage.deleteKeyCache(KeyStore.TOKEN);
        localStorage.clearSettings();

        tvUserName.setText("");
        tvUserName.setVisibility(View.GONE);
        tvHello.setVisibility(View.GONE);

        KepapayaApplication.showToast(this, R.string.logged_out, Toast.LENGTH_SHORT);
    }

    public void initFragment(Fragment fragment) {
        Fragment currentFragment = getSupportFragmentManager().
                findFragmentById(R.id.main_fragments_container);
        String fragmentName = fragment.getClass().getName();
        if (currentFragment != null) {
            String currentFragmentName = currentFragment.getClass().getName();
            if (!currentFragmentName.equals(fragmentName)) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .replace(R.id.main_fragments_container, fragment)
                        .addToBackStack("my_fragment").commit();
            }
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .add(R.id.main_fragments_container, fragment)
                    .commit();
        }
    }
}