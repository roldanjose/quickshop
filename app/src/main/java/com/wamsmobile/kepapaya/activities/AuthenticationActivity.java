package com.wamsmobile.kepapaya.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.fragments.authentication.LoginFragment;

/**
 * Created by dell on 29/05/2017.
 */

public class AuthenticationActivity extends AppCompatActivity {

    boolean commingFromSignOff;
    private View statubar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        initComponents();
        chooseInitialFragment();
    }

    private void initComponents() {
        statubar = findViewById(R.id.status_bar);
        updateStatusBarColor(R.color.blackTransparent3);
        try {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        } catch (NullPointerException npe) {
            Log.v("","", npe);
        }
    }

    public void updateStatusBarColor(int colorResID) {// Color resource ID
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, colorResID));
        }
        statubar.setBackgroundColor(ContextCompat.getColor(this, colorResID));
    }

    public void chooseInitialFragment() {
        /*commingFromSignOff = getIntent().getBooleanExtra("SIGNOFF", commingFromSignOff);
        if (commingFromSignOff) {
            commingFromSignOff = false;
            // Open login fragment

        } else {
            // Open fragment of the menu
        }*/
        initFragment(new LoginFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().
                findFragmentById(R.id.authentication_fragments_container);
        //TODO: Cambiar LoginFragment por el fragment segun la dinánica
        if (fragment instanceof LoginFragment) {
            finish();
            System.exit(0);
        }
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public boolean isEmpty(EditText editText) {
        return editText.getText().toString().equals("")
                || editText.getText().toString().isEmpty();

    }

    public void initFragment(Fragment fragment) {
        Fragment currentFragment = getSupportFragmentManager().
                findFragmentById(R.id.authentication_fragments_container);
        String fragmentName = fragment.getClass().getName();
        if (currentFragment != null) {
            String currentFragmentName = currentFragment.getClass().getName();
            if (!currentFragmentName.equals(fragmentName)) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .replace(R.id.authentication_fragments_container, fragment)
                        .addToBackStack("my_fragment").commit();
            }
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.authentication_fragments_container, fragment)
                    .commit();
        }
    }

}
