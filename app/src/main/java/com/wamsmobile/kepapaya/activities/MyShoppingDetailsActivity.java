package com.wamsmobile.kepapaya.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wamsmobile.kepapaya.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;


/**
 * Created by WAMS 5 on 23/03/2017.
 */

public class MyShoppingDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    // Views
    private TextView cartTransactionId, carDatePay, carDateDelay,
            carCharged, dispatchText, dispatchEmail, dispatchPhone;
    private ImageView dispatch_img;
    private Button btnBack;

    private String bundleIdTransactionPass, bundleColony;
    private long bundleCartDatePay, bundleCartDateDelivery;
    private Double bundleCartCharged;
    private Double bundleCoordinateX, bundleCoordinateY;

    // Google Maps
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shopping_details);
        initComponents();
        getShoppingPayData();
        setShoppingPayData();
    }

    private void setShoppingPayData() {
        cartTransactionId.setText(bundleIdTransactionPass);
        carDatePay.setText(getFormattedDateFromTimestamp(bundleCartDatePay));
        carDateDelay.setText(getFormattedDateFromTimestamp(bundleCartDateDelivery));
        String cartCharged = Double.toString(bundleCartCharged);
        carCharged.setText(cartCharged);
        dispatchText.setText(bundleColony);
    }

    private void initComponents() {
        cartTransactionId = findViewById(R.id.txt_cart_transactation_id);
        carDatePay = findViewById(R.id.txt_cart_date_pay);
        carDateDelay = findViewById(R.id.txt_cart_date_delay);
        carCharged = findViewById(R.id.txt_car_charged);
        dispatchText = findViewById(R.id.txt_dispatch_test);
        dispatchEmail = findViewById(R.id.txt_dispatch_email);
        dispatchPhone = findViewById(R.id.txt_dispatch_phone);
        dispatch_img = findViewById(R.id.dispatch_img);
        mMapView = findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    // Method to convert to a date format
    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds) {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        return new SimpleDateFormat("MMM d, yyyy", Locale.getDefault()).format(date);

    }

    public void getShoppingPayData() {
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();

            bundleIdTransactionPass = bundle.getString("idTransactionPass");
            bundleCartDatePay = (long) bundle.getFloat("cartDatePay");
            bundleCartDateDelivery = (long) bundle.getFloat("cartDateDelivery");
            bundleCartCharged = decimalsRounder(bundle.getDouble("cartCharged"), 2);

            // Dispatch by
            bundleColony = bundle.getString("dispatchTest");
            // bundle_phone = bundle.getString("dispatchPhone");
            // bundle_email = bundle.getString("dispatchEmail");

            // Maps
            bundleCoordinateX = bundle.getDouble("mapCoordinateX");
            bundleCoordinateY = bundle.getDouble("mapCoordinateY");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);

        mGoogleMap = googleMap;
        getShoppingPayData();

        LatLng latLng = new LatLng(bundleCoordinateY, bundleCoordinateX);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        CameraPosition cameraPosition = CameraPosition.builder().target
                (new LatLng(bundleCoordinateY, bundleCoordinateX)).zoom(15).bearing(5).tilt(45).build();
        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
