package com.wamsmobile.kepapaya.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.GooglePlayServiceCheck;
import com.wamsmobile.kepapaya.libs.MarshmallowPermission;
import com.wamsmobile.kepapaya.libs.MyApplication;
import com.wamsmobile.kepapaya.libs.RestClient360;
import com.wamsmobile.kepapaya.pojos.CitiesModel;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.pojos.EditAddress;
import com.wamsmobile.kepapaya.pojos.ServiceZoneModel;
import com.wamsmobile.kepapaya.pojos.SetAddress;
import com.wamsmobile.kepapaya.pojos.StatesModel;
import com.wamsmobile.kepapaya.utilities.EditTextView;
import com.wamsmobile.kepapaya.utilities.Label;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamsmobile.kepapaya.interfaces.KeyStore.FROM_BUTTON;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMyLocationButtonClickListener {

    private static final String TAG = AddAddressActivity.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private static GoogleApiClient mGoogleApiClient;

    private Label toolbarTitle;
    private Toolbar toolbar;
    private EditTextView edtCountry, edtState, edtTownship, edtLocation, edtStreet, edtBuilding,
            edtHouse, edtPoint, edtOther, edtInstructions, edtPhone, edtMobile, edtAddresAlias;
    private ImageView transparentImage;
    private ImageButton btnPositionFocus, btnMarkerFocus, btnContinue;
    private ScrollView scrollView;

    private String country, state, city, township, location, street, building, house, point, other,
            instructions, phone, mobile, alias, mlatitude, mlongitude, commingFrom, addressID,
            countryID, stateID, cityID;

    private ProgressBar progressBar;
    private List<String> statesList, citiesList;
    private List<LatLng> zoneList;
    private StatesModel statesResponse;
    private CitiesModel citiesResponse;
    private ServiceZoneModel zoneResponse;
    private Marker mMarker;
    private String token = "vkl1DQW84";
    private String tokenSecret = "579e4e3d-070c-49f5-972d-493ce814c96b";

    /*  Required for Google Maps  */
    private GoogleMap googleMap;
    private LocationRequest mLocationRequest;
    private Location mLocation;
    private MapView mapView;
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        initComponents();

        // At activity startup we manually check the internet status and show
        // the status message
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        setUpGoogleMap(savedInstanceState);//Set Up Map
        mapEventScroll();
        statesRequest();

        chooseToolbarTitle();
        receiveAddressData();
    }

    private void initComponents() {
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        } catch (NullPointerException npe) {
            Log.v("", "", npe);
        }

        edtCountry = findViewById(R.id.edt_country);
        edtState = findViewById(R.id.edt_state);
        edtTownship = findViewById(R.id.edt_township);
        edtLocation = findViewById(R.id.edt_location);
        edtStreet = findViewById(R.id.edt_street);
        edtBuilding = findViewById(R.id.edt_building);
        edtHouse = findViewById(R.id.edt_house);
        edtPoint = findViewById(R.id.edt_point);
        edtOther = findViewById(R.id.edt_other);
        edtInstructions = findViewById(R.id.edt_instructions);
        edtPhone = findViewById(R.id.edt_phone);
        edtMobile = findViewById(R.id.edt_mobile);
        edtAddresAlias = findViewById(R.id.edt_address_alias);
        btnPositionFocus = findViewById(R.id.btn_position_focus);
        btnMarkerFocus = findViewById(R.id.btn_marker_focus);
        transparentImage = findViewById(R.id.transparent_image);
        btnContinue = findViewById(R.id.btn_continue);
        scrollView = findViewById(R.id.scrollView);
        progressBar = findViewById(R.id.progress_bar);
    }

    private void chooseToolbarTitle() {
        commingFrom = getIntent().getStringExtra(FROM_BUTTON);
        if (commingFrom.equals(KeyStore.ADD_ADDRESS)) {
            toolbarTitle.setText(R.string.add_address);
        } else if (commingFrom.equals(KeyStore.EDIT_MENU)) {
            toolbarTitle.setText(R.string.edit_address);
        }
    }

    // Set on Click listener to this buttons
    private void setListener() {
        btnContinue.setOnClickListener(this);
        btnMarkerFocus.setOnClickListener(this);
        btnPositionFocus.setOnClickListener(this);
    }

    /* Set Up google map if Google Play Services Exists */
    private void setUpGoogleMap(Bundle savedInstanceState) {
        if (new GooglePlayServiceCheck().isGooglePlayInstalled(AddAddressActivity.this)) {
            mapView = findViewById(R.id.map_view);
            mapView.onCreate(savedInstanceState);
            mapView.getMapAsync(this);//after getting map call async method, this method will call onMapReady(GoogleMap map) method
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        //When map ready
        googleMap = map;

        googleMap.getUiSettings().setZoomControlsEnabled(true);//enable zoom controls
        googleMap.getUiSettings().setCompassEnabled(true);//enable compass
        googleMap.getUiSettings().setMapToolbarEnabled(false);//disable toolbar

        //Enable some gestures
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);

        //Enable current Location buttons
        enableCurrentLocationButton();

        //Set Normal Map
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        zoneDataCall();

        /* Set Click listener for Marker and MyLocationButton Click event*/
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMyLocationButtonClickListener(this);

        clickUpdateMarkerPosition();

        /* Set on Click listener to all buttons*/
        setListener();

        if (commingFrom.equals(KeyStore.EDIT_MENU)) {
            Double mlatitude = Double.parseDouble(getIntent().getStringExtra("LATITUDE"));
            Double mlongitude = Double.parseDouble(getIntent().getStringExtra("LONGITUDE"));
            LatLng latLng = new LatLng(mlatitude, mlongitude);
            // Add a marker on Address position
            updateCurrentMarker(latLng, getIntent().getStringExtra("ADDRESS"), 17.0f);
        } else if (commingFrom.equals(KeyStore.ADD_ADDRESS)) {
            LatLng latLng = new LatLng(19.4326077, -99.13320799999997);
            // Add a marker on Address position
            updateCurrentMarker(latLng, getString(R.string.inside_the_delivery_zone), 17.0f);
        }
    }

    private void enableCurrentLocationButton() {
        //before further proceed check if google map is null or not because this method is calling after giving permission
        if (googleMap != null) {
            if (ContextCompat.checkSelfPermission(AddAddressActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddAddressActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                MarshmallowPermission.requestLocationPermission(AddAddressActivity.this);
            else {
                googleMap.setMyLocationEnabled(true);//enable blue dot
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);//enable Location button, if you don't want MyLocationButton set it false
            }
        }
    }

    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(AddAddressActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(AddAddressActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                MarshmallowPermission.requestLocationPermission(AddAddressActivity.this);
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }

    /* Show Location Access Dialog */
    private void showSettingDialog() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        mLocationRequest.setInterval(30 * 1000);//after 30sec the location will update
        mLocationRequest.setFastestInterval(5 * 1000);//5 sec Time interval
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //getOriginLatLng();
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(AddAddressActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    protected void onStop() {
        //On Stop disconnect Api Client
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //On API Client Connection check if location permission is granted or not
        if (ContextCompat.checkSelfPermission(AddAddressActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            MarshmallowPermission.requestLocationPermission(AddAddressActivity.this);
        else {
            //If granted then get last know location and update the location label if last know location is not null
            mLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLocation != null) {

                //locationLabel.setText(String.format(getResources().getString(R.string.lat_lng), mLocation.getLatitude(), mLocation.getLongitude()));
            }
        }
    }

    //Handle API Client Connection Suspended
    @Override
    public void onConnectionSuspended(int i) {
    }

    //Handle API Client Connection Failed
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    //Handle Marker click event
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    //On Location change method
    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    //Update current marker of map
    private void updateCurrentMarker(LatLng latLng, String markerTitle, float cameraZoom) {
        if (googleMap != null) {
            mMarker = googleMap.addMarker(new MarkerOptions()
                    .title(getIntent().getStringExtra(markerTitle))//set Marker title
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                    .position(latLng)
                    .draggable(true).visible(true));
            // Animating to the touched position
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, cameraZoom));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        //If result is OK then start location Update
                        //startLocationUpdates();
                        //Toast.makeText(this, "GPS is Enabled in your device", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_CANCELED:
                        //else show toast
                        //Toast.makeText(this, "GPS is Disabled in your device", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
        }
    }

    //Stop Location Update Method
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    //Handle MyLocationButton click event
    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    /* On Request permission method to check the
    permisison is granted or not for Marshmallow+ Devices  */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //If permission granted show location dialog if APIClient is not null
                    if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();

                    //enable MyLocationButton
                    enableCurrentLocationButton();

                } else {
                    //Toast.makeText(AddAddressActivity.this, "Acceso Denegado", Toast.LENGTH_SHORT).show();
                    // permission denied
                }
                return;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        //On Pause stop Location Update
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            stopLocationUpdates();
        MyApplication.activityPaused();// On Pause notify the Application
    }

    @Override
    protected void onResume() {
        //On Resume check if google api is not null and is connected so that location update should start again
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            // startLocationUpdates();
        } else {
            //else if not connected or null initiate api client
            initGoogleAPIClient();
        }

        //Check Location permission
        checkPermissions();
        registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Register broadcast receiver to check the status of GPS
        MyApplication.activityResumed();// On Resume notify the Application
        mapView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        mapView = null;
        //Unregister receiver on destroy
        if (gpsLocationReceiver != null)
            unregisterReceiver(gpsLocationReceiver);
    }

    /* Diactivate general ScrollView when map is scrolling */
    public void mapEventScroll() {
        transparentImage.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    public void getAddressInfo() {
        country = edtCountry.getText().toString();

        //TODO:...
        state = edtState.getText().toString();
        //TODO: ...
        city = "";

        township = edtTownship.getText().toString();
        location = edtLocation.getText().toString();
        street = edtStreet.getText().toString();
        building = edtBuilding.getText().toString();
        house = edtHouse.getText().toString();
        point = edtPoint.getText().toString();
        other = edtOther.getText().toString();
        instructions = edtInstructions.getText().toString();
        phone = edtPhone.getText().toString();
        mobile = edtMobile.getText().toString();
        alias = edtAddresAlias.getText().toString();

        mlatitude = Double.toString(mMarker.getPosition().latitude);
        mlongitude = Double.toString(mMarker.getPosition().longitude);
    }


    public void clickUpdateMarkerPosition() {
        // Setting a click event handler for the map
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng latLng) {
                // Clears the previously marker
                if (mMarker != null) {
                    mMarker.remove();
                }
                // // Placing a marker on the touched position
                updateCurrentMarker(new LatLng(latLng.latitude, latLng.longitude),
                        edtTownship.getText().toString(), 17.0f);
            }
        });
    }

    public void zoneDataCall() {
        Call<ServiceZoneModel> callZone = RestClient360.buildService().getServiceZone360();
        callZone.enqueue(new Callback<ServiceZoneModel>() {
            @Override
            public void onResponse(Call<ServiceZoneModel> call, Response<ServiceZoneModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        zoneResponse = response.body();
                        zoneList = new ArrayList<>();
                        for (int i = 0; i < zoneResponse.getArea().size(); i++) {
                            LatLng latLng = new LatLng(zoneResponse.getArea().get(i).get(1),
                                    zoneResponse.getArea().get(i).get(0));
                            zoneList.add(latLng);
                        }
                        delimiteZone();
                        // Add User Address
                        //Toast.makeText(AddAddressActivity.this, "Zona Cargada", Toast.LENGTH_LONG).show();
                    } else {
                        if (response.errorBody() != null) {
                            String error = response.errorBody().toString();
                            Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceZoneModel> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void delimiteZone() {
        // Instantiates a new Polygon object and adds points to define a rectangle
        PolygonOptions polygonOptions = new PolygonOptions()
                .strokeColor(Color.argb(117, 255, 0, 140))
                .strokeWidth(2)//(Color.RED)
                .fillColor(Color.argb(117, 255, 0, 140));
        for (LatLng latLng : zoneList) {
            polygonOptions.add(latLng);
        }
        googleMap.addPolygon(polygonOptions);
    }

    public void createAddressRequest() {
        SetAddress setAddress = new SetAddress(token, tokenSecret, alias, location,
                street, township, house, building, stateID, cityID, countryID, mlatitude, mlongitude);

        Call<EMesResponse> call = RestClient360.buildService().setUserAddress360(setAddress);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        // Add User Address
                        Toast.makeText(AddAddressActivity.this, R.string.address_added_successfully, Toast.LENGTH_LONG).show();
                        Intent intentRefresh = new Intent(KeyStore.REFRESH_ADDRESSES);
                        LocalBroadcastManager.getInstance(AddAddressActivity.this).sendBroadcast(intentRefresh);
                        finish();
                    } else {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        String error = response.body().getMes();
                        Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                    String error = response.errorBody().toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void addressUpdateRequest() {
        EditAddress addressEdit = new EditAddress(addressID, token, tokenSecret, alias, location,
                street,  township, house, building, state, city, country, mlatitude, mlongitude);

        Call<EMesResponse> call = RestClient360.buildService().editUserAddress360(addressEdit);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        // Add User Address
                        Toast.makeText(AddAddressActivity.this, R.string.address_updated_successfully, Toast.LENGTH_LONG).show();
                        Intent intentRefresh = new Intent(KeyStore.REFRESH_ADDRESSES);
                        LocalBroadcastManager.getInstance(AddAddressActivity.this).sendBroadcast(intentRefresh);
                        finish();
                    } else {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        String error = response.body().getMes();
                        Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                    String error = response.errorBody().toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                if (commingFrom.equals(KeyStore.ADD_ADDRESS)) {
                    if (!progressBar.isShown()) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    getAddressInfo();
                    createAddressRequest();
                } else if (commingFrom.equals(KeyStore.EDIT_MENU)) {
                    if (!progressBar.isShown()) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    getAddressInfo();
                    addressUpdateRequest();
                }
                break;
            case R.id.btn_marker_focus:
                if (mMarker != null) {
                    LatLng latLng = new LatLng(mMarker.getPosition().latitude, mMarker.getPosition().longitude);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
                } else
                    Toast.makeText(this, R.string.no_marker, Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_position_focus:
                // Clears the previously marker
                if (mMarker != null) {
                    mMarker.remove();
                }
                LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                // Add a marker on your current position
                updateCurrentMarker(latLng, "Estás Aquí", 17.0f);
                break;
        }
    }

    public void receiveAddressData() {
        if (commingFrom.equals(KeyStore.EDIT_MENU)) {
            edtCountry.setText(getIntent().getStringExtra("COUNTRY"));
            edtLocation.setText(getIntent().getStringExtra("LOCATION"));
            edtStreet.setText(getIntent().getStringExtra("STREET"));
            edtTownship.setText(getIntent().getStringExtra("TOWNSHIP"));
            edtState.setText(getIntent().getStringExtra("STATE"));
            edtBuilding.setText(getIntent().getStringExtra("BUILDING"));
            edtHouse.setText(getIntent().getStringExtra("HOUSE"));
            addressID = getIntent().getStringExtra("ADDRESSID");
        }
    }

    public void statesRequest() {
        countryID = "57858198746be78f6a04ae56";
        Call<StatesModel> callState = RestClient360.buildService().getStatesResponse360(countryID);
        callState.enqueue(new Callback<StatesModel>() {
            @Override
            public void onResponse(Call<StatesModel> call, Response<StatesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        statesResponse = response.body();
                        // Spinner Drop down element
                        statesList = new ArrayList<String>();
                        for (int i = 0; i < statesResponse.getStates().size(); i++) {
                            statesList.add(statesResponse.getStates().get(i).getName());
                        }
                        state = statesList.get(0);
                        edtState.setText(state);
                        stateID = statesResponse.getStates().get(0).getId();
                        citiesRequest();
                        if (commingFrom.equals(KeyStore.EDIT_MENU)) {
                            state = getIntent().getStringExtra("STATE");
                        }
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                    } else {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        String error = response.errorBody().toString();
                        Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                    String error = response.errorBody().toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<StatesModel> call, Throwable t) {
                if (progressBar.isShown()) {
                    progressBar.setVisibility(View.GONE);
                }
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void citiesRequest() {
        Call<CitiesModel> callCity = RestClient360.buildService().getCitiesResponse360(stateID);
        callCity.enqueue(new Callback<CitiesModel>() {
            @Override
            public void onResponse(Call<CitiesModel> call, Response<CitiesModel> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        citiesResponse = response.body();
                        // Spinner Drop down element
                        citiesList = new ArrayList<String>();
                        for (int i = 0; i < citiesResponse.getCities().size(); i++) {
                            citiesList.add(citiesResponse.getCities().get(i).getName());
                        }
                        city = citiesList.get(0);
                        cityID = citiesResponse.getCities().get(0).getId();

                        if (commingFrom.equals(KeyStore.EDIT_MENU)) {
                            city = getIntent().getStringExtra("CITY");
                        }
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                    } else {
                        if (progressBar.isShown()) {
                            progressBar.setVisibility(View.GONE);
                        }
                        String error = response.errorBody().toString();
                        Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                    String error = response.errorBody().toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<CitiesModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    if (progressBar.isShown()) {
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(AddAddressActivity.this, error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}