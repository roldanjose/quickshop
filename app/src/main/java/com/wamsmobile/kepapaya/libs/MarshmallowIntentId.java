package com.wamsmobile.kepapaya.libs;

/**
 * Created by dell on 18/04/2017.
 */

public class MarshmallowIntentId {
    //Intent id's for Marshmallow Permissions
    public static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
}