package com.wamsmobile.kepapaya.libs;

import com.wamsmobile.kepapaya.interfaces.ServicesInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dell on 21/02/2017.
 */

public class RestClient {

    public static Retrofit buildRetrofit(){
        final String url = "https://quickshop.com.mx/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static ServicesInterface buildService(){
        ServicesInterface service = buildRetrofit().create(ServicesInterface.class);
        return service;
    }

}
