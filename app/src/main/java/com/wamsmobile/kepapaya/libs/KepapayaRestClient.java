package com.wamsmobile.kepapaya.libs;

import com.wamsmobile.kepapaya.interfaces.ServicesInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dell on 21/02/2017.
 */

public class KepapayaRestClient {

    public static Retrofit buildRetrofit(){
        final String url = "http://165.227.96.113:5000/";
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ServicesInterface buildService(){
        return buildRetrofit().create(ServicesInterface.class);
    }

}
