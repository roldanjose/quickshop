package com.wamsmobile.kepapaya.libs;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocalStorage {

	private final String SHARED_PREFS_FILE = "HMPrefs";
	private Context mContext;

	/**
	 * Constructor 
	 * @param context
	 */
	public LocalStorage(Context context){
		mContext = context;
	}
	
	/**
	 * Get setting's preferences
	 * @return SharedPreferences data type
	 */
	private SharedPreferences getSettings(){
		return mContext.getSharedPreferences(SHARED_PREFS_FILE, 0);
	}

	/**
	 * @param key
	 * @return the value according to the key
	 */
	public String getPermanentVariable(String key){
		return getSettings().getString(key, null);
	}
	/**
	 * @param key
	 * @return the stored value according to the boolean key
	 */
	public Boolean getPermanentBoolean(String key){
		return getSettings().getBoolean(key, true);
	}
	/**
	 * @param key
	 * @return the stored value according to int key
	 */
	public int getPermanentInteger(String key){
		return getSettings().getInt(key, 0);
	}

	/**
	 * Store the variables in key value format
	 * @param key
	 * @param value
	 */
	public void setPermanentVariable(String key, String value){
		SharedPreferences.Editor editor = getSettings().edit();
		editor.putString(key, value );
		editor.apply();
	}
	
	/**
	 * Store an especific boolean value
	 * @param key
	 * @param value
	 */
	public void setPermanentBoolean(String key, Boolean value){
		SharedPreferences.Editor editor = getSettings().edit();
		editor.putBoolean(key, value);
		editor.apply();
	}
	/**
	 * Store an specific int value
	 * @param key
	 * @param value
	 */
	public void setPermanentInteger(String key, int value){
		SharedPreferences.Editor editor = getSettings().edit();
		editor.putInt(key, value);
		editor.apply();
	}
	
	/**
	 * Delete all the SharedPreferences variables
	 */
	public void clearSettings(){
		SharedPreferences.Editor editor = getSettings().edit();
		editor.clear();
		editor.apply();
	}

	/**
	 * Delete the value of an especific key
	 * @param key
	 */
	public void deleteKeyCache(String key){
		SharedPreferences.Editor editor = getSettings().edit();
		editor.remove(key);
		editor.apply();
	}

	/**
	 * Store an Json Objet
	 * @param key
	 * @param object
	 */
    public void saveJSONObject(String key, JSONObject object) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(key, object.toString());
        editor.apply();
    }

    /**
     * @param key
     * @return an Json Objet
     * @throws JSONException
     */
    public JSONObject loadJSONObject(String key) throws JSONException {
        return new JSONObject(getSettings().getString(key, "{}"));
    }	
    
	/**
	 * Store an Json Array
	 * @param key
	 * @param array
	 */
    public void saveJSONArray(String key, JSONArray array) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(key, array.toString());
        editor.apply();
    }

    /**
     * @param key
     * @return an Json Array
     * @throws JSONException
     */
    public JSONArray loadJSONArray(String key) throws JSONException {
        return new JSONArray(getSettings().getString(key, "[]"));
    }	    

}
