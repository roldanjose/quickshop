package com.wamsmobile.kepapaya.libs;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class GooglePlayServiceCheck {

    /*  Check the existence of Google Play Services  */
    public boolean isGooglePlayInstalled(AppCompatActivity appCompatActivity) {
        // Getting status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appCompatActivity);

        // Showing status
        if (status == ConnectionResult.SUCCESS) {
            return true;
        } else {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, appCompatActivity, requestCode);
            dialog.show();
            return false;
        }
    }
}
