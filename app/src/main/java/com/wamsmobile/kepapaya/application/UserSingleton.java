package com.wamsmobile.kepapaya.application;

import com.wamsmobile.kepapaya.pojos.UserResponse;

/**
 * Created by dell on 08/02/2018.
 */

public final class UserSingleton {

    private static final UserSingleton USER_SINGLETON = new UserSingleton();
    private static UserResponse userResponse;

    private UserSingleton() {

    }

    public static UserSingleton get(){
        return USER_SINGLETON;
    }

    public UserResponse getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(UserResponse userResponse) {
        UserSingleton.userResponse = userResponse;
    }
}