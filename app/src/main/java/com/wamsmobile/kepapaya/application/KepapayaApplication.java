package com.wamsmobile.kepapaya.application;

import android.app.Activity;
import android.app.Application;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.utilities.Label;

/**
 * Created by dell on 23/04/2018.
 */

public class KepapayaApplication extends Application {

    private static Toast toast;

    public static void showToast(final Activity activity, final int toastTextResId, final int toastDuration) {

        showToast(activity, activity.getString(toastTextResId), toastDuration);
    }

    public static void showToast(final Activity activity, final String toastTextString, final int toastDuration) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = activity.getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast,
                        (ViewGroup) activity.findViewById(R.id.relative_toast));
                Label message = layout.findViewById(R.id.txt_toast_message);
                message.setText(toastTextString);
                toast = new Toast(activity.getApplicationContext());
                toast.setGravity(Gravity.FILL_HORIZONTAL, 0, 0);
                toast.setDuration(toastDuration);
                toast.setView(layout);
                toast.show();
            }
        });
    }

    public static void hideToast() {
        if (toast != null) {
            if (toast.getView().getWindowVisibility() == View.VISIBLE) {
                toast.cancel();
            }
        }
    }

    // Round Double
    public static double decimalsRounder(double inicialValue, int decimalNumbers) {
        double intPart, result;
        result = inicialValue;
        intPart = Math.floor(result);
        result = (result - intPart) * Math.pow(10, decimalNumbers);
        result = Math.round(result);
        result = (result / Math.pow(10, decimalNumbers)) + intPart;
        return result;
    }
}
