
package com.wamsmobile.kepapaya.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("items")
    @Expose
    private List<ItemCartModel> items = null;
    @SerializedName("isItYou")
    @Expose
    private Boolean isItYou;
    @SerializedName("total")
    @Expose
    private Double total;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Member() {
    }

    /**
     * 
     * @param total
     * @param items
     * @param isItYou
     * @param user
     */
    public Member(User user, List<ItemCartModel> items, Boolean isItYou, Double total) {
        super();
        this.user = user;
        this.items = items;
        this.isItYou = isItYou;
        this.total = total;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ItemCartModel> getItems() {
        return items;
    }

    public void setItems(List<ItemCartModel> items) {
        this.items = items;
    }

    public Boolean getIsItYou() {
        return isItYou;
    }

    public void setIsItYou(Boolean isItYou) {
        this.isItYou = isItYou;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
