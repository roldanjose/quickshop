package com.wamsmobile.kepapaya.pojos;

/**
 * Created by dell on 17/05/2017.
 */

public class Email {
    private String mail;

    public Email(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
}
