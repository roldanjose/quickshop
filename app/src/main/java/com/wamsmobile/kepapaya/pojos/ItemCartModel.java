
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemCartModel {

    @SerializedName("item")
    @Expose
    private Item_ item;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ItemCartModel() {
    }

    /**
     * 
     * @param item
     * @param quantity
     */
    public ItemCartModel(Item_ item, Integer quantity) {
        super();
        this.item = item;
        this.quantity = quantity;
    }

    public Item_ getItem() {
        return item;
    }

    public void setItem(Item_ item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
