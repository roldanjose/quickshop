
package com.wamsmobile.kepapaya.pojos;

import java.io.Serializable;
import java.util.List;

public class Dispatch implements Serializable {

    public String id;
    public List<Transaction> transactions = null;
    public String listName;
    public Float listTotal;
    public List<Member> members = null;
    public long payedDate;
    public long deliveryDate;
    public DispatchAddress dispatchAddress;
    public Integer status;
    public Boolean yours;

    public Dispatch(String listName) {
        this.listName = listName;
    }
}
