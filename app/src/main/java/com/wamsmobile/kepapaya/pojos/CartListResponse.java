
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartListResponse {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("list")
    @Expose
    private List list;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CartListResponse() {
    }

    /**
     * 
     * @param e
     * @param list
     */
    public CartListResponse(Integer e, List list) {
        super();
        this.e = e;
        this.list = list;
    }

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
