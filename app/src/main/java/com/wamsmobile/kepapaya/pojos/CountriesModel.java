
package com.wamsmobile.kepapaya.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountriesModel {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("countries")
    @Expose
    private List<Country> countries = null;

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

}
