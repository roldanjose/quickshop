
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item_ {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("searchName")
    @Expose
    private String searchName;
    @SerializedName("descriptionShort")
    @Expose
    private String descriptionShort;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("unit")
    @Expose
    private Integer unit;
    @SerializedName("meassure")
    @Expose
    private String meassure;
    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("gtin")
    @Expose
    private String gtin;
    @SerializedName("createdAt")
    @Expose
    private Double createdAt;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("urlShort")
    @Expose
    private String urlShort;
    @SerializedName("favorite")
    @Expose
    private Object favorite;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("rank")
    @Expose
    private Integer rank;
    @SerializedName("inList")
    @Expose
    private Integer inList;
    @SerializedName("inDispatch")
    @Expose
    private Integer inDispatch;
    @SerializedName("inListAvg")
    @Expose
    private Double inListAvg;
    @SerializedName("inDispatchAvg")
    @Expose
    private Double inDispatchAvg;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("brandName")
    @Expose
    private String brandName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Item_() {
    }

    /**
     * 
     * @param inListAvg
     * @param inList
     * @param gtin
     * @param favorite
     * @param brandName
     * @param inDispatch
     * @param url
     * @param size
     * @param id
     * @param categoryName
     * @param unit
     * @param rank
     * @param category
     * @param price
     * @param urlShort
     * @param tax
     * @param createdAt
     * @param name
     * @param searchName
     * @param inDispatchAvg
     * @param active
     * @param brand
     * @param images
     * @param meassure
     * @param descriptionShort
     */
    public Item_(String id, String name, String searchName, String descriptionShort, String category, String brand, Integer size, Integer unit, String meassure, List<String> images, Double price, Double tax, String gtin, Double createdAt, String url, String urlShort, Object favorite, Integer active, Integer rank, Integer inList, Integer inDispatch, Double inListAvg, Double inDispatchAvg, String categoryName, String brandName) {
        super();
        this.id = id;
        this.name = name;
        this.searchName = searchName;
        this.descriptionShort = descriptionShort;
        this.category = category;
        this.brand = brand;
        this.size = size;
        this.unit = unit;
        this.meassure = meassure;
        this.images = images;
        this.price = price;
        this.tax = tax;
        this.gtin = gtin;
        this.createdAt = createdAt;
        this.url = url;
        this.urlShort = urlShort;
        this.favorite = favorite;
        this.active = active;
        this.rank = rank;
        this.inList = inList;
        this.inDispatch = inDispatch;
        this.inListAvg = inListAvg;
        this.inDispatchAvg = inDispatchAvg;
        this.categoryName = categoryName;
        this.brandName = brandName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public String getMeassure() {
        return meassure;
    }

    public void setMeassure(String meassure) {
        this.meassure = meassure;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public Double getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Double createdAt) {
        this.createdAt = createdAt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlShort() {
        return urlShort;
    }

    public void setUrlShort(String urlShort) {
        this.urlShort = urlShort;
    }

    public Object getFavorite() {
        return favorite;
    }

    public void setFavorite(Object favorite) {
        this.favorite = favorite;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getInList() {
        return inList;
    }

    public void setInList(Integer inList) {
        this.inList = inList;
    }

    public Integer getInDispatch() {
        return inDispatch;
    }

    public void setInDispatch(Integer inDispatch) {
        this.inDispatch = inDispatch;
    }

    public Double getInListAvg() {
        return inListAvg;
    }

    public void setInListAvg(Double inListAvg) {
        this.inListAvg = inListAvg;
    }

    public Double getInDispatchAvg() {
        return inDispatchAvg;
    }

    public void setInDispatchAvg(Double inDispatchAvg) {
        this.inDispatchAvg = inDispatchAvg;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }


}
