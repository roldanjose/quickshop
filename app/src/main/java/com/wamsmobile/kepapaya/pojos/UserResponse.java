
package com.wamsmobile.kepapaya.pojos;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("e")
    @Expose
    private Integer e;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("mes")
    @Expose
    private String mes;

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

}
