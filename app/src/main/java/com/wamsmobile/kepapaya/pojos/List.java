
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("createdAt")
    @Expose
    private Double createdAt;
    @SerializedName("members")
    @Expose
    private java.util.List<Member> members = null;
    @SerializedName("isOwner")
    @Expose
    private Boolean isOwner;
    @SerializedName("total")
    @Expose
    private Double total;

    /**
     * No args constructor for use in serialization
     * 
     */
    public List() {
    }

    /**
     * 
     * @param total
     * @param id
     * @param createdBy
     * @param status
     * @param createdAt
     * @param name
     * @param isOwner
     * @param type
     * @param members
     */
    public List(String id, String name, Integer type, Integer status, CreatedBy createdBy, Double createdAt, java.util.List<Member> members, Boolean isOwner, Double total) {
        super();
        this.id = id;
        this.name = name;
        this.type = type;
        this.status = status;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.members = members;
        this.isOwner = isOwner;
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public Double getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Double createdAt) {
        this.createdAt = createdAt;
    }

    public java.util.List<Member> getMembers() {
        return members;
    }

    public void setMembers(java.util.List<Member> members) {
        this.members = members;
    }

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(Boolean isOwner) {
        this.isOwner = isOwner;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
