package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dell on 24/02/2017.
 */

public class LoginResponse {

    @SerializedName("e")
    @Expose
    private Integer e;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("tokenSecret")
    @Expose
    private String tokenSecret;

    @SerializedName("expiresAt")
    @Expose
    private String expiresAt;

    @SerializedName("mes")
    @Expose
    private String mes;

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Integer getE() {
        return e;
    }
    public void setE(Integer e) {
        this.e = e;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getTokenSecret() {
        return tokenSecret;
    }
    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }
    public String getExpiresAt() {
        return expiresAt;
    }
    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }
}
