
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetAddress {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("tokensecret")
    @Expose
    private String tokensecret;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("numext")
    @Expose
    private String numext;
    @SerializedName("numint")
    @Expose
    private String numint;
    @SerializedName("addresscontact")
    @Expose
    private String addresscontact;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("postal")
    @Expose
    private String postal;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("colony")
    @Expose
    private String colony;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;

    public SetAddress(String token, String tokensecret, String alias, String numext, String numint, String address, String addresscontact, String postal, String state, String city, String colony, String lat, String lng) {
        this.token = token;
        this.tokensecret = tokensecret;
        this.alias = alias;
        this.numext = numext;
        this.numint = numint;
        this.addresscontact = addresscontact;
        this.address = address;
        this.postal = postal;
        this.state = state;
        this.city = city;
        this.colony = colony;
        this.lat = lat;
        this.lng = lng;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokensecret() {
        return tokensecret;
    }

    public void setTokensecret(String tokensecret) {
        this.tokensecret = tokensecret;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNumext() {
        return numext;
    }

    public void setNumext(String numext) {
        this.numext = numext;
    }

    public String getNumint() {
        return numint;
    }

    public void setNumint(String numint) {
        this.numint = numint;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressContact() {
        return addresscontact;
    }

    public void setAddressContact(String addresscontact) {
        this.addresscontact = addresscontact;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getLatitud() {
        return lat;
    }

    public void setLatitud(String lat) {
        this.lat = lat;
    }

    public String getLongitud() {
        return lng;
    }

    public void setLongitud(String lng) {
        this.lng = lng;
    }

}
