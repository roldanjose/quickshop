package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by WAMS 5 on 20/03/2017.
 */

public class ShoppingPayResponse {

    @SerializedName("e")
    @Expose
    private Integer e;

    @SerializedName("dispatch")
    @Expose
    private List<Dispatch> dispatch;

    @SerializedName("mes")
    @Expose
    private String mes;

    public ShoppingPayResponse(Integer e, Dispatch dispatch, String mes) {
        this.e = e;
        this.mes = mes;
    }
    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public ShoppingPayResponse(List<Dispatch> dispatch) {
        this.dispatch = dispatch;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public List<Dispatch> getDispatch() {
        return dispatch;
    }

    public void setDispatch(List<Dispatch> dispatch) {
        this.dispatch = dispatch;
    }

}
