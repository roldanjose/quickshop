
package com.wamsmobile.kepapaya.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CitiesModel {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("cities")
    @Expose
    private List<Cities> cities = null;

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List<Cities> getCities() {
        return cities;
    }

    public void setCities(List<Cities> cities) {
        this.cities = cities;
    }

}
