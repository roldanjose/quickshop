
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Member_ {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("isItYou")
    @Expose
    private Boolean isItYou;
    @SerializedName("total")
    @Expose
    private Double total;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Boolean getIsItYou() {
        return isItYou;
    }

    public void setIsItYou(Boolean isItYou) {
        this.isItYou = isItYou;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
