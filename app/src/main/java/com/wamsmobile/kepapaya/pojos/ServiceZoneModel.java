
package com.wamsmobile.kepapaya.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceZoneModel {

    @SerializedName("e")
    @Expose
    private Integer e;
    @SerializedName("area")
    @Expose
    private List<List<Double>> area = null;

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public List<List<Double>> getArea() {
        return area;
    }

    public void setArea(List<List<Double>> area) {
        this.area = area;
    }

}
