package com.wamsmobile.kepapaya.interfaces;

import com.wamsmobile.kepapaya.models.AddressesModel;
import com.wamsmobile.kepapaya.models.ChangePasswordModel;
import com.wamsmobile.kepapaya.models.Login;
import com.wamsmobile.kepapaya.models.Register;
import com.wamsmobile.kepapaya.models.ShoppingCartTokenModel;
import com.wamsmobile.kepapaya.models.TokensUser;
import com.wamsmobile.kepapaya.models.UpdateDataModel;
import com.wamsmobile.kepapaya.pojos.CartListResponse;
import com.wamsmobile.kepapaya.pojos.ChangePasswordResponse;
import com.wamsmobile.kepapaya.pojos.CitiesModel;
import com.wamsmobile.kepapaya.pojos.CountriesModel;
import com.wamsmobile.kepapaya.pojos.DispatchResponse;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.pojos.EditAddress;
import com.wamsmobile.kepapaya.pojos.Email;
import com.wamsmobile.kepapaya.pojos.FavoritesResponse;
import com.wamsmobile.kepapaya.pojos.FindIdTransitionResponse;
import com.wamsmobile.kepapaya.pojos.LoginResponse;
import com.wamsmobile.kepapaya.pojos.RegisterResponse;
import com.wamsmobile.kepapaya.pojos.ServiceZoneModel;
import com.wamsmobile.kepapaya.pojos.SetAddress;
import com.wamsmobile.kepapaya.pojos.StatesModel;
import com.wamsmobile.kepapaya.pojos.UserResponse;
import com.wamsmobile.kepapaya.responseDataModel.Category;
import com.wamsmobile.kepapaya.responseDataModel.GenericResponse;
import com.wamsmobile.kepapaya.responseDataModel.ProductsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dell on 21/02/2017.
 */

public interface ServicesInterface {

    /**
     * Kepapaya services interface
     */
    @GET("api/products/category")
    Call<GenericResponse<List<Category>>> getCategories();

    @GET("api/products/item")
    Call<GenericResponse<ProductsResponse>> getProducts(@Query("name") String searchTerm,
                                                        @Query("category") String categoryID,
                                                        @Query("page") String page);


    /**
     * Quickshop and Balance_360 services interface
     */

    @POST("api/users")
    Call<RegisterResponse> userResgister(@Body Register data);

    @PUT("api/users/mail/{token}")
    Call<RegisterResponse> sendTokenValidation(@Path("token") String data);

    @POST("api/users/login")
    Call<LoginResponse> loginUser(@Body Login data);

    @GET("api/users/")
    Call<UserResponse> getUserInfo(@Query("token") String token,
                                   @Query("tokensecret") String tokensecret);

    @PUT("api/lists/item/{idItem}")
    Call<EMesResponse> sendShoppingCartRequest(@Path("idItem") String data,
                                               @Body ShoppingCartTokenModel model);

    @GET("api/lists/cart")
    Call<CartListResponse> getCartList(@Query("token") String token,
                                       @Query("tokensecret") String secretToken,
                                       @Query("start") int start,
                                       @Query("max") int max);

    @POST("api/history/item/{idItem}")
    Call<EMesResponse> addToFavorites(@Path("idItem") String id,
                                      @Body TokensUser tokensUser);

    @DELETE("api/history/item/{idItem}")
    Call<EMesResponse> deleteFromFavorites(@Path("idItem") String id,
                                           @Query("token") String token,
                                           @Query("tokensecret") String tokenSecret);

    @GET("api/history/item")
    Call<FavoritesResponse> getFavorites (@Query("token") String token,
                                          @Query("tokensecret") String tokensecret);

    @PUT("api/users/particular")
    Call<EMesResponse> updateProfile(@Body UpdateDataModel updateDataModel);

    @GET("api/dispatches/details/{orderid}")
    Call<FindIdTransitionResponse> findIdOrder(@Path("orderid") String orderid,
                                               @Query("token") String token,
                                               @Query("tokensecret") String tokensecret);

    @PUT("api/users/password")
    Call<ChangePasswordResponse> changePassword(@Body ChangePasswordModel changePasswordModel);

    @GET("api/dispatches")
    Call<DispatchResponse> getDispatches(@Query("token") String token,
                                         @Query("tokensecret") String tokensecret,
                                         @Query("max") int max,
                                         @Query("start") int start,
                                         @Query("status") int status);

    @DELETE("api/lists/item/{idItem}")
    Call<EMesResponse> deleteItemFromList(@Path("idItem") String id,
                                          @Query("token") String token,
                                          @Query("tokensecret") String tokenSecret,
                                          @Query("quantity") int quantity);

    @GET("api/users/addresses")
    Call<AddressesModel> getAddressesList360(@Query("token") String token,
                                             @Query("tokensecret") String tokenSecret);

    @GET("api/data/countries")
    Call<CountriesModel> getCountriesResponse360();

    @GET("api/data/states/{countryId}")
    Call<StatesModel> getStatesResponse360(@Path("countryId") String countryId);

    @GET("api/data/cities/{stateId}")
    Call<CitiesModel> getCitiesResponse360(@Path("stateId") String stateId);

    @GET("api/site/served")
    Call<ServiceZoneModel> getServiceZone360();

    @POST("api/users/addresses")
    Call<EMesResponse> setUserAddress360(@Body SetAddress addressSet);

    @PUT("api/users/addresses")
    Call<EMesResponse> editUserAddress360(@Body EditAddress addressEdit);

    @DELETE("api/users/addresses")
    Call<EMesResponse> deleteUserAddress360(@Query("oldaddress") String addressID,
                                            @Query("token") String token,
                                            @Query("tokensecret") String tokenSecret);

    @POST("api/users/forgotpassword")
    Call<EMesResponse> forgotPassword(@Body Email email);
}