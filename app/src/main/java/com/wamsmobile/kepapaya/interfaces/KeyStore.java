package com.wamsmobile.kepapaya.interfaces;

/**
 * Created by dell on 08/02/2018.
 */

public interface KeyStore {

    String TOKEN = "TOKEN";
    String TOKEN_SECRET = "TOKEN_SECRET";
    String REFRESH_ADDRESSES = "REFRESH_ADDRESSES";
    String FROM_BUTTON = "FROM_BUTTON";
    String ADD_ADDRESS = "ADD_ADDRESS";
    String EDIT_MENU = "EDIT_MENU";
    String LOGGED_IN = "LOGGED_IN";
}
