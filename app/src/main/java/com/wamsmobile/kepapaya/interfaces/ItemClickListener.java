package com.wamsmobile.kepapaya.interfaces;

import android.view.View;

/**
 * Created by dell on 24/05/2017.
 */

public interface ItemClickListener {
    void onClick(View view, int position);
}
