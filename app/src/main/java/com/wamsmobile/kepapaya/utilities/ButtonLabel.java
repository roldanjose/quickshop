package com.wamsmobile.kepapaya.utilities;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RemoteViews.RemoteView;

@RemoteView
public class ButtonLabel extends Label {

    public ButtonLabel(Context context) {
        this(context, null);
    }

    public ButtonLabel(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.buttonStyle);
    }

    public ButtonLabel(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ButtonLabel(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public CharSequence getAccessibilityClassName() {
        return ButtonLabel.class.getName();
    }
}
