package com.wamsmobile.kepapaya.utilities;

/**
 * Created by dell on 26/06/2017.
 */

public enum FontType {

    AVENIR_BOOK("fonts/Avenir-Book.otf"),
    AVENIR_LIGHT("fonts/Avenir-Light.otf"),
    AVENIR_ROMAN("fonts/Avenir-Roman.otf"),
    AVENIR_HEAVY("fonts/Avenir-Heavy.otf"),
    MODAK_REGULAR("fonts/Modak-Regular.otf");

    private String path;

    FontType(String path) {
        this.path = path;
    }

    public String getAssetPath() {
        return path;
    }
}