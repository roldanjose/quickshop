package com.wamsmobile.kepapaya.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wamsmobile.kepapaya.R;

/**
 * Created by dell on 14/02/2018.
 */

public class CategoriesHolder extends Holder {

    public TextView tvCategory;
    public ImageView imgCategory;

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public CategoriesHolder(View itemView) {
        super(itemView);
        tvCategory = itemView.findViewById(R.id.tv_category);
        imgCategory = itemView.findViewById(R.id.img_category);
    }
}
