package com.wamsmobile.kepapaya.holders;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.utilities.Label;

/**
 * Created by dell on 03/04/2018.
 */

public class ProductHolder extends Holder{

    public LinearLayout linearInfo;
    public Label tvProductName, tvPrice, tvSizeMeasure, tvAvailability, tvDescription;
    public ImageView imgProduct, imgAvailabilityIcon;
    public ImageButton carShopPlusBtn, heartFavoriteBtn, btnShowInfo;

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public ProductHolder(View itemView) {
        super(itemView);
        linearInfo = itemView.findViewById(R.id.linear_info);
        tvProductName = itemView.findViewById(R.id.tv_name_and_quantity);
        tvPrice = itemView.findViewById(R.id.tv_price);
        tvSizeMeasure = itemView.findViewById(R.id.tv_size_measure);
        tvAvailability = itemView.findViewById(R.id.tv_availability);
        tvDescription = itemView.findViewById(R.id.tv_description);
        imgProduct = itemView.findViewById(R.id.img_product);
        imgAvailabilityIcon = itemView.findViewById(R.id.img_availability_icon);
        carShopPlusBtn = itemView.findViewById(R.id.btn_add_to_carshop);
        heartFavoriteBtn = itemView.findViewById(R.id.heart_favorite_btn);
        btnShowInfo = itemView.findViewById(R.id.btn_show_info);
    }
}
