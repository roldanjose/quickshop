package com.wamsmobile.kepapaya.holders;

import android.view.View;
import android.widget.ImageView;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.utilities.Label;

/**
 * Created by dell on 14/02/2018.
 */

public class ShoppingCartHolder extends Holder {

    public Label tvNameQuantity, subTotal;
    public ImageView removeBtn;

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public ShoppingCartHolder(View itemView) {
        super(itemView);
        tvNameQuantity = itemView.findViewById(R.id.tv_name_and_quantity);
        subTotal = itemView.findViewById(R.id.txt_subtotal);
        removeBtn =itemView.findViewById(R.id.btn_remove);
    }
}
