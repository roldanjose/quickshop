package com.wamsmobile.kepapaya.holders;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wamsmobile.kepapaya.R;

/**
 * Created by dell on 09/02/2018.
 */

public class AffiliatedAddressesHolder extends Holder {

    public ImageButton btnDelete;
    public TextView tvAlias, tvAddress, tvColony, tvContact;

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public AffiliatedAddressesHolder(View itemView) {
        super(itemView);
        btnDelete = itemView.findViewById(R.id.btn_delete_address);
        tvAlias = itemView.findViewById(R.id.tv_address_alias);
        tvAddress = itemView.findViewById(R.id.tv_address);
        tvColony = itemView.findViewById(R.id.tv_colony);
        tvContact = itemView.findViewById(R.id.tv_contact);
    }
}
