package com.wamsmobile.kepapaya.fragments.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;

/**
 * Created by dell on 30/05/2017.
 */

public class SuccessfulRegistrationFragment extends Fragment implements View.OnClickListener {

    private AuthenticationActivity activity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_successful_registration, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view){
        activity = (AuthenticationActivity) getActivity();

        Button btnContinue = view.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(this);
   }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue: {

                break;
            }
        }
    }
}
