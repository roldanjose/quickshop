package com.wamsmobile.kepapaya.fragments.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.adapters.ShoppingCartAdapter;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.pojos.CartListResponse;
import com.wamsmobile.kepapaya.pojos.ItemCartModel;
import com.wamsmobile.kepapaya.pojos.Member;
import com.wamsmobile.kepapaya.pojos.User;
import com.wamsmobile.kepapaya.utilities.Label;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;

/**
 * Created by Willow on 23/3/2017.
 */

public class FragmentShoppingCart extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private List<User> listUser = new ArrayList<>();
    private List<Member> listMember = new ArrayList<>();
    private HashMap<User, List<ItemCartModel>> myHashMapModel = new HashMap<>();

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView recyclerView;
    private ShoppingCartAdapter adapter;
    private Label tvTotal;
    private ImageButton btnContinue;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_cart, container, false);
        initComponents(view);
        loadCartList();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter("refresh"));
        return view;
    }

    private void initComponents(View view) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.updateStatusBarColor(R.color.colorPrimaryDark);
        setToolbarConfig(mainActivity);

        swipeRefresh = view.findViewById(R.id.swiperefresh_list);
        recyclerView = view.findViewById(R.id.recycler_shopping_cart);
        swipeRefresh.setOnRefreshListener(this);
        tvTotal = view.findViewById(R.id.tv_total);
        btnContinue = view.findViewById(R.id.btn_continue);
        //TODO: ...
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mainActivity.initFragment(new );
            }
        });
    }

    private void setToolbarConfig(final MainActivity mainActivity) {
        mainActivity.getToolbar().setVisibility(View.VISIBLE);
        mainActivity.setToolbarTitle(R.string.shopping_cart);
        mainActivity.getToolbar().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.colorPrimary));
        mainActivity.setToolbarFirstIndicator(R.drawable.bt_dspl);
        mainActivity.setToolbarTitleColorById(android.R.color.white);
        mainActivity.setToolbarFirstIndicatorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onClick(v);
            }
        });
    }

    public void loadCartList() {
        int startList = 0;
        int maxLists = 20;

        LocalStorage localStorage = new LocalStorage(getContext());
        String token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        String tokenSecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);
        swipeRefresh.setRefreshing(true);

        Call<CartListResponse> call = RestClient.buildService().getCartList(token, tokenSecret, startList, maxLists);
        call.enqueue(new Callback<CartListResponse>() {
            @Override
            public void onResponse(Call<CartListResponse> call, Response<CartListResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        CartListResponse listResponse = response.body();
                        listUser = getDataListUser(listResponse);
                        myHashMapModel = getDataHashMap(listResponse);
                        listMember = getListMember(listResponse);
                        List<ItemCartModel> itemCartModels = myHashMapModel.get(listUser.get(0));
                        adapter = new ShoppingCartAdapter(getActivity(), itemCartModels, R.layout.item_shopping_cart);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        swipeRefresh.setRefreshing(false);

                        Double totalToPay = listMember.get(0).getTotal();
                        totalToPay = decimalsRounder(totalToPay, 2);
                        tvTotal.setText("$" + totalToPay);
                    } else {
                        swipeRefresh.setRefreshing(false);
                        String error = "error";
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    swipeRefresh.setRefreshing(false);
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CartListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    swipeRefresh.setRefreshing(false);
                    String error = t.toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public List<User> getDataListUser(CartListResponse response) {

        com.wamsmobile.kepapaya.pojos.List modelList = response.getList();
        List<Member> membersModel = new ArrayList<>();
        List<User> listUserReturn = new ArrayList<>();
        membersModel = modelList.getMembers();
        for (Member members : membersModel) {
            listUserReturn.add(members.getUser());
        }
        return listUserReturn;
    }

    public HashMap<User, List<ItemCartModel>> getDataHashMap(CartListResponse response) {

        com.wamsmobile.kepapaya.pojos.List modelList = response.getList();
        List<Member> membersModel = new ArrayList<>();
        HashMap<User, List<ItemCartModel>> hashMap = new HashMap<>();
        List<ItemCartModel> itemCartModels = new ArrayList<>();

        membersModel = modelList.getMembers();
        for (Member members : membersModel) {
            User user = members.getUser();
            itemCartModels = members.getItems();
            hashMap.put(user, itemCartModels);
        }

        return hashMap;
    }

    public List<Member> getListMember(CartListResponse response) {

        com.wamsmobile.kepapaya.pojos.List modelList = response.getList();
        List<Member> membersModel = new ArrayList<>();

        membersModel = modelList.getMembers();
        return membersModel;
    }

    @Override
    public void onRefresh() {
        loadCartList();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            loadCartList();
        }
    };
}
