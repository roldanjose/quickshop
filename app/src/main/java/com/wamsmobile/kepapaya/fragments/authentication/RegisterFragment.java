package com.wamsmobile.kepapaya.fragments.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.Register;
import com.wamsmobile.kepapaya.pojos.RegisterResponse;
import com.wamsmobile.kepapaya.utilities.EditTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 30/05/2017.
 */

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private AuthenticationActivity activity;

    private EditTextView edtName, edtLastName, edtID, edtPhone,
            edtEmail, edtPassword, edtConfirm;

    private ImageButton btnContinue;
    private ProgressBar progressBar;

    private TextInputLayout tilName, tilLastName, tilID, tilPhone,
            tilEmail, tilPassword, tilConfirm;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initComponents(view);
        setOnClickListeners();
        return view;
    }

    private void initComponents(View view) {
        activity = (AuthenticationActivity) getActivity();

        edtName = view.findViewById(R.id.edt_name);
        edtLastName = view.findViewById(R.id.edt_last_name);
        edtID = view.findViewById(R.id.edt_id);
        edtPhone = view.findViewById(R.id.edt_phone);
        edtEmail = view.findViewById(R.id.edt_email);
        edtPassword = view.findViewById(R.id.edt_password);
        edtConfirm = view.findViewById(R.id.edt_confirm);
        btnContinue = view.findViewById(R.id.btn_continue);
        progressBar = view.findViewById(R.id.progress_bar);

        tilName = view.findViewById(R.id.name_input_layout);
        tilLastName = view.findViewById(R.id.last_name_input_layout);
        tilID = view.findViewById(R.id.id_input_layout);
        tilPhone = view.findViewById(R.id.phone_input_layout);
        tilEmail = view.findViewById(R.id.email_input_layout);
        tilPassword = view.findViewById(R.id.password_input_layout);
        tilConfirm = view.findViewById(R.id.confirm_input_layout);
    }

    private void setOnClickListeners() {
        btnContinue.setOnClickListener(this);
    }

    private boolean isValidPassword(String pass) {
        return pass.length() >= 8;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                registerRequest();
                break;
        }
    }

    private void registerRequest() {
        boolean isEmptyName = activity.isEmpty(edtName);
        boolean isEmptyLastName = activity.isEmpty(edtLastName);
        boolean isEmptyRfc = activity.isEmpty(edtID);
        boolean isEmptyPhone = activity.isEmpty(edtPhone);
        boolean isEmptyEmail = activity.isEmpty(edtEmail);
        boolean isEmptyPassword = activity.isEmpty(edtPassword);
        boolean isEmptyConfirm = activity.isEmpty(edtConfirm);

        if (isEmptyName) {
            tilName.setErrorEnabled(true);
            tilName.setError(getString(R.string.required_field));
        } else if (isEmptyLastName) {
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getString(R.string.required_field));
        } else if (isEmptyRfc) {
            tilID.setErrorEnabled(true);
            tilID.setError(getString(R.string.required_field));
        } else if (isEmptyPhone) {
            tilPhone.setErrorEnabled(true);
            tilPhone.setError(getString(R.string.required_field));
        } else if (isEmptyEmail) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.required_field));
        } else if (isEmptyPassword) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.required_field));
        } else if (isEmptyConfirm) {
            tilConfirm.setErrorEnabled(true);
            tilConfirm.setError(getString(R.string.required_field));
        } else {
            String name = edtName.getText().toString().trim();
            String lastname = edtLastName.getText().toString().trim();
            String id = edtID.getText().toString().trim();
            String phone = edtPhone.getText().toString().trim();
            String email = edtEmail.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                tilEmail.setErrorEnabled(true);
                tilEmail.setError(getString(R.string.invalid_email));
            } else {
                if (!isValidPassword(password)) {
                    tilPassword.setErrorEnabled(true);
                    tilPassword.setError(getString(R.string.should_contain));
                } else {
                    progressBar.setVisibility(View.VISIBLE);

                    Register registerDatos = new Register();

                    Call<RegisterResponse> call = RestClient.buildService().userResgister(registerDatos);
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {

                            if (response.isSuccessful()) {
                                progressBar.setVisibility(View.GONE);
                                if (response.body().getE() == 0) {
                                    activity.initFragment(new SuccessfulRegistrationFragment());
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    String error = response.body().getMes();
                                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                progressBar.setVisibility(View.GONE);
                                String error = response.errorBody().toString();
                                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            Log.i("retro_mes", t.toString());
                            progressBar.setVisibility(View.GONE);
                            String error = t.toString();
                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }
    }
}
