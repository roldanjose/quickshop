package com.wamsmobile.kepapaya.fragments.main;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.adapters.CategoriesAdapter;
import com.wamsmobile.kepapaya.adapters.CollectionAdapter;
import com.wamsmobile.kepapaya.application.KepapayaApplication;
import com.wamsmobile.kepapaya.holders.CategoriesHolder;
import com.wamsmobile.kepapaya.libs.KepapayaRestClient;
import com.wamsmobile.kepapaya.responseDataModel.Category;
import com.wamsmobile.kepapaya.responseDataModel.GenericResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 17/02/2017.
 */

public class FragmentCategories extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private MainActivity mainActivity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        initComponents(view);
        categoriesRequest();
        return view;
    }

    private void initComponents(View view) {
        mainActivity = (MainActivity) getActivity();
        mainActivity.updateStatusBarColor(R.color.blackTransparent3);
        setToolbarConfig();

        swipeRefreshLayout = view.findViewById(R.id.swiperefresh_categories);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.recycler_categories);

        ImageButton btnSearch = view.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentProducts fragment = new FragmentProducts();
                mainActivity.initFragment(fragment);
            }
        });
    }

    private void setToolbarConfig() {
        mainActivity.getToolbar().setVisibility(View.VISIBLE);
        mainActivity.setToolbarTitle(R.string.categories);
        mainActivity.getToolbar().setBackgroundColor(ContextCompat.getColor(mainActivity, android.R.color.white));
        mainActivity.setToolbarFirstIndicator(R.drawable.bt_dspl);
        mainActivity.setToolbarTitleColorById(R.color.colorPrimary);
        mainActivity.setToolbarFirstIndicatorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onClick(v);
            }
        });
    }

    public void categoriesRequest() {
        swipeRefreshLayout.setRefreshing(true);

        Call call = KepapayaRestClient.buildService().getCategories();
        call.enqueue(new Callback<GenericResponse<List<Category>>>(){
            @Override
            public void onResponse(Call<GenericResponse<List<Category>>> call, Response<GenericResponse<List<Category>>> response) {
                if (response.isSuccessful()) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.body().getE() == 0) {
                        List<Category> categories = response.body().getData();
                        CategoriesAdapter adapter2 = new CategoriesAdapter
                                (getActivity(), categories, R.layout.item_categories);
                        adapter2.setOnItemClickListener(new CollectionAdapter.OnItemClickListener<Category, CategoriesHolder>() {
                            @Override
                            public void onItemClick(Category category, CategoriesHolder holder, int position) {
                                FragmentProducts fragment = new FragmentProducts();
                                Bundle bundle = new Bundle();
                                bundle.putString("CATEGORYID", category.getId());
                                fragment.setArguments(bundle);
                                FragmentProducts.newInstance(category.getId());
                                mainActivity.initFragment(fragment);
                            }
                        });
                        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                        recyclerView.setAdapter(adapter2);
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        String error = response.body().getData().toString();
                        KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
                    }
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    String error = response.errorBody().toString();
                    KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<List<Category>>> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                swipeRefreshLayout.setRefreshing(false);
                String error = t.toString();
                KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onRefresh() {
        categoriesRequest();
    }
}
