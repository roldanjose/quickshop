package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.adapters.CollectionAdapter;
import com.wamsmobile.kepapaya.adapters.ProductsAdapter;
import com.wamsmobile.kepapaya.application.KepapayaApplication;
import com.wamsmobile.kepapaya.libs.KepapayaRestClient;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.responseDataModel.GenericResponse;
import com.wamsmobile.kepapaya.responseDataModel.Pagination;
import com.wamsmobile.kepapaya.responseDataModel.Product;
import com.wamsmobile.kepapaya.responseDataModel.ProductsResponse;
import com.wamsmobile.kepapaya.utilities.EditTextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * Created by Willow on 13/3/2017.
 */

public class FragmentProducts extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, CollectionAdapter.OnScrollEndListener {

    private MainActivity mainActivity;
    private String searchTerm = "", categoryID = "";
    private int page;
    private Pagination pagination;
    private List<Product> productList;
    private ProductsAdapter adapter;

    private Call call;
    private ProgressBar loadMore, progressBar;
    private SwipeRefreshLayout swipeRefresh;
    private boolean onRefreshed;
    private RecyclerView recyclerView;
    private RequestManager requestManager;
    private EditTextView edtSearch;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        initComponents(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        configRecyclerView();
    }

    private void initComponents(View view) {
        mainActivity = (MainActivity) getActivity();
        mainActivity.updateStatusBarColor(R.color.colorPrimaryDark);
        setToolbarConfig();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            categoryID = bundle.getString("CATEGORYID");
        }

        LocalStorage localStorage = new LocalStorage(getContext());

        requestManager = Glide.with(this);
        swipeRefresh = view.findViewById(R.id.swiperefresh_products);
        swipeRefresh.setOnRefreshListener(this);
        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        loadMore = view.findViewById(R.id.progress_bar_loadmore);
        recyclerView = view.findViewById(R.id.recycler_products);
        edtSearch = view.findViewById(R.id.edt_search);
        edtSearch.setHint(R.string.search);

        //loadFavorites();
        load("");
        theSearch();
    }

    private void setToolbarConfig() {
        mainActivity.getToolbar().setVisibility(View.VISIBLE);
        mainActivity.setToolbarTitle(getString(R.string.products));
        mainActivity.setToolbarTitleColorById(android.R.color.white);
        mainActivity.getToolbar().setBackgroundColor
                (ContextCompat.getColor(mainActivity, R.color.colorPrimary));
        mainActivity.setToolbarFirstIndicator(R.drawable.bt_dspl);
        mainActivity.setToolbarFirstIndicatorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onClick(v);
            }
        });
        mainActivity.setToolbarLastIndicator(R.drawable.ic_shp);
    }

    private void configRecyclerView() {
        productList = new ArrayList<>();
        adapter = new ProductsAdapter
                (getContext(), productList, R.layout.item_products, requestManager);
        adapter.setOnScrollEndedListener(this);
        adapter.setItemViewCacheSize(20);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    public static FragmentProducts newInstance(String s) {
        Bundle bundle = new Bundle();
        bundle.putString("", s);
        FragmentProducts fragment = new FragmentProducts();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void load(String name) {
        page = 1;
        call = KepapayaRestClient.buildService().getProducts(name, categoryID, String.valueOf(page));
        call.enqueue(new Callback<GenericResponse<ProductsResponse>>() {
            @Override
            public void onResponse(Call<GenericResponse<ProductsResponse>> call, Response<GenericResponse<ProductsResponse>> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    swipeRefresh.setRefreshing(false);
                    ProductsResponse data = response.body().getData();
                    if (response.body().getE() == 0) {
                        if (onRefreshed) {
                            productList.clear();
                            productList.addAll(data.getItems());
                            adapter.notifyDataSetChanged();
                            onRefreshed = false;
                        } else {
                            productList.addAll(data.getItems());
                            adapter.notifyDataSetChanged();
                            pagination = data.getPagination();
                            pagination.getTotalPages();
                        }
                    } else {
                        swipeRefresh.setRefreshing(false);
                        String error = data.toString();
                        KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    swipeRefresh.setRefreshing(false);
                    String error = response.errorBody().toString();
                    KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<ProductsResponse>> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    swipeRefresh.setRefreshing(false);
                    String error = t.toString();
                    KepapayaApplication.showToast(getActivity(), error, Toast.LENGTH_SHORT);
                }
            }
        });
    }

    public void loadMore() {
        page =+ 1;
        call = KepapayaRestClient.buildService().getProducts(searchTerm, categoryID, String.valueOf(page));
        call.enqueue(new Callback<GenericResponse<ProductsResponse>>() {
            @Override
            public void onResponse(Call<GenericResponse<ProductsResponse>> call,
                                   Response<GenericResponse<ProductsResponse>> response) {

                if (response.isSuccessful()) {
                    //remove loading view
                    loadMore.setVisibility(View.GONE);
                    ProductsResponse data = response.body().getData();
                    List<Product> result = data.getItems();
                    if (result.size() > 0) {
                        //add loaded data
                        productList.addAll(result);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<GenericResponse<ProductsResponse>> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void theSearch() {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                call.cancel();
                load(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onRefresh() {
        swipeRefresh.setRefreshing(true);
        onRefreshed = true;
        load(searchTerm);
    }

    /*public void loadFavorites() {
        Call<FavoritesResponse> call = RestClient.buildService().getFavorites(token, tokensecret);
        call.enqueue(new Callback<FavoritesResponse>() {
            @Override
            public void onResponse(Call<FavoritesResponse> call, Response<FavoritesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        FavoritesResponse favoritesResponse = response.body();
                        List<FavoriteItem> favoriteItems = favoritesResponse.getFavoriteItems();
                    }
                } else {
                    if (response.errorBody() != null) {
                        String error = response.errorBody().toString();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<FavoritesResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }*/

    @Override
    public void onScrollEnd(int size, int position) {
        loadMore.setVisibility(View.VISIBLE);
        if (size == pagination.getItemsCount()) {
            if (pagination.getTotalPages() > page)
                // a method which requests remote data
                loadMore();
        }
    }
}