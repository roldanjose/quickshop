package com.wamsmobile.kepapaya.fragments.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.Register;
import com.wamsmobile.kepapaya.pojos.RegisterResponse;
import com.wamsmobile.kepapaya.utilities.EditTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 30/05/2017.
 */

public class ForgotFragment extends Fragment implements View.OnClickListener {

    private AuthenticationActivity activity;

    private EditTextView edtCode, edtPassword, edtConfirm;
    private ImageButton btnContinue;
    private ProgressBar progressBar;

    private TextInputLayout tilCode, tilPassword, tilConfirm;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forgot, container, false);
        initComponents(view);
        setOnClickListeners();
        return view;
    }

    private void initComponents(View view) {
        activity = (AuthenticationActivity) getActivity();

        edtCode = view.findViewById(R.id.edt_code);
        edtPassword = view.findViewById(R.id.edt_password);
        edtConfirm = view.findViewById(R.id.edt_confirm);
        btnContinue = view.findViewById(R.id.btn_continue);
        progressBar = view.findViewById(R.id.progress_bar);

        tilCode = view.findViewById(R.id.code_input_layout);
        tilPassword = view.findViewById(R.id.password_input_layout);
        tilConfirm = view.findViewById(R.id.confirm_input_layout);
    }

    private void setOnClickListeners() {
        btnContinue.setOnClickListener(this);
    }

    private boolean isValidPassword(String pass) {
        return pass.length() >= 8;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                registerRequest();
                break;
        }
    }

    private void registerRequest() {
        boolean isEmptyCode = activity.isEmpty(edtCode);
        boolean isEmptyPassword = activity.isEmpty(edtPassword);
        boolean isEmptyConfirm = activity.isEmpty(edtConfirm);

        if (isEmptyCode) {
            tilCode.setError(getString(R.string.required_field));
        } else if (isEmptyPassword) {
            tilPassword.setError(getString(R.string.required_field));
        } else if (isEmptyConfirm) {
            tilConfirm.setError(getString(R.string.required_field));
        } else {
            String code = edtCode.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            if (!isValidPassword(password)) {
                edtPassword.setError(getString(R.string.should_contain));
            } else {
                progressBar.setVisibility(View.VISIBLE);

                Register registerDatos = new Register();

                Call<RegisterResponse> call = RestClient.buildService().userResgister(registerDatos);
                call.enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {

                        if (response.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            if (response.body().getE() == 0) {
                                activity.initFragment(new SuccessfulRegistrationFragment());
                            } else {
                                progressBar.setVisibility(View.GONE);
                                String error = response.body().getMes();
                                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            String error = response.errorBody().toString();
                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        Log.i("retro_mes", t.toString());
                        progressBar.setVisibility(View.GONE);
                        String error = t.toString();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }
}
