package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.application.UserSingleton;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.UpdateDataModel;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.pojos.User;
import com.wamsmobile.kepapaya.pojos.UserResponse;
import com.wamsmobile.kepapaya.utilities.EditTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by WAMS 5 on 15/03/2017.
 */

public class FragmentMyProfile extends Fragment {

    private MainActivity activity;
    //    Variables para el Header
    private String token, tokensecret;

    private EditTextView edtUpdateName, edtUpdateLastname, edtUpdateRFC, edtUpdatePhone, edtUpdateBirthday;
    private ImageButton btnContinue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_profile_fragment, container, false);
        initComponents(view);
        fillProfile();

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfileRequest();
            }
        });

        return view;
    }

    private void initComponents(View view) {
        activity = (MainActivity) getActivity();
        activity.setToolbarTitle(getString(R.string.my_profile));
        LocalStorage localStorage = new LocalStorage(getContext());
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokensecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        edtUpdateName = view.findViewById(R.id.edt_update_name);
        edtUpdateLastname = view.findViewById(R.id.edt_update_lastname);
        edtUpdateRFC = view.findViewById(R.id.edt_id);
        edtUpdatePhone = view.findViewById(R.id.edt_update_phone);
        edtUpdateBirthday = view.findViewById(R.id.edt_update_birthday);
        btnContinue = view.findViewById(R.id.btn_continue);
    }

    private void updateProfileRequest() {
        String name = edtUpdateName.getText().toString();
        String lastname = edtUpdateLastname.getText().toString();
        String rfc = edtUpdateRFC.getText().toString();
        String phone = edtUpdatePhone.getText().toString();
        String birthday = edtUpdateBirthday.getText().toString();

        UpdateDataModel updateDataModel = new UpdateDataModel
                (token, tokensecret, name, lastname, rfc, phone, birthday);

        Call<EMesResponse> call = RestClient.buildService().updateProfile(updateDataModel);
        call.enqueue(new Callback<EMesResponse>() {
            @Override
            public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        // Profile data successfully updated.
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EMesResponse> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });
    }

    // Fill user data
    private void fillProfile() {
        UserResponse userResponse = UserSingleton.get().getUserResponse();
        User user = userResponse.getUser();
        edtUpdateName.setText(user.getName());
        edtUpdateLastname.setText(user.getLastName());
        edtUpdateRFC.setText(user.getRfc());
        edtUpdatePhone.setText(user.getPhone());
        edtUpdateBirthday.setText(user.getDoB());
    }

}
