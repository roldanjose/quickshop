package com.wamsmobile.kepapaya.fragments.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.ShoppingCartTokenModel;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.utilities.Label;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamsmobile.kepapaya.R.id.tv_quantity;
import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;

/**
 * Custom Dialog Fragment that prompts the user to add items to his cart
 */

public class FragmentDialogShoppingcartAdd extends DialogFragment implements View.OnClickListener {

    public static final String ARG_ITEM_ID = "FragmentDialogShoppingcartAdd";

    private Label tvProductName, tvBrand, tvQuantity, tvPrice,
            tvSubtotal, tvCategory, tvSize, tvProductMeassure, edtQuantity;
    private ImageView imgProduct;
    private ImageButton btnMinus, btnPlus, btnCancel, btnAdd;
    private ProgressBar progressBar, progressBarImage;

    private String price, token, tokenSecret, productId, commingFrom, productName,
            productImage, brandName, category, size, meassure;
    private float subtotal;
    private int productsQuantity;

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());

        /* WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
         Display display = wm.getDefaultDisplay();
         Point size = new Point();
         display.getSize(size);
         int width = size.x;
         double ImageSize = width * 0.5;*/

        initComponents(dialog);
        setListeners();
        setProductData();

        inputQuantityManually();
        calculateSubtotal();
        displayQuantity();

        return dialog;
    }


    private void initComponents(Dialog dialog) {
        // Dialog configuration
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_VERTICAL);

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        window.setBackgroundDrawable(new ColorDrawable(0x7000000));
        layoutParams.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(layoutParams);

        // Set the layout for the dialog
        dialog.setContentView(R.layout.fragment_dialog_shoppingcar_add);
        dialog.setCancelable(true);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LocalStorage localStorage = new LocalStorage(getContext());
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokenSecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        // Find views
        tvProductName = dialog.findViewById(R.id.tv_name_and_quantity);
        tvProductMeassure = dialog.findViewById(R.id.tv_product_size);
        imgProduct = dialog.findViewById(R.id.img_product);
        edtQuantity = dialog.findViewById(R.id.edt_quantity);
        tvQuantity = dialog.findViewById(tv_quantity);
        tvPrice = dialog.findViewById(R.id.tv_price);
        tvSubtotal = dialog.findViewById(R.id.txt_subtotal);
        btnMinus = dialog.findViewById(R.id.btn_minus);
        btnPlus = dialog.findViewById(R.id.btn_plus);
        btnCancel = dialog.findViewById(R.id.btn_cancel);
        btnAdd = dialog.findViewById(R.id.btn_add);
        progressBar = dialog.findViewById(R.id.progress_bar_dialog);
        progressBarImage = dialog.findViewById(R.id.progress_bar_image);

        getBundleArgs();
        setProductData();
    }

    private void setListeners() {
        btnMinus.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
    }

    private void getBundleArgs() {
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            productId = bundle.getString("ID");
            productName = bundle.getString("NAME");
            meassure = bundle.getString("MEASSURE");
            productImage = bundle.getString("IMAGE");
            productsQuantity = bundle.getInt("QUANTITY");
            price = bundle.getString("PRICE");
            category = bundle.getString("CATEGORY");
            commingFrom = bundle.getString("FRAGMENT");
        }
    }

    private void setProductData() {
        tvProductName.setText(productName);
        tvProductMeassure.setText(meassure);
        Glide.with(getActivity())
                .load(productImage)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBarImage.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imgProduct);
        edtQuantity.setText(Integer.toString(productsQuantity));
        edtQuantity.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100")});
        tvQuantity.setText(Integer.toString(productsQuantity));
        tvPrice.setText("$" + price);
        tvSubtotal.setText("$" + subtotal);
    }

    /**
     * This method is called when the plus button is clicked.
     */
    public void increment() {
        if (productsQuantity < 100) {
            productsQuantity = productsQuantity + 1;
            displayQuantity();
        }
    }

    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement() {
        if (productsQuantity > 1) {
            productsQuantity = productsQuantity - 1;
            displayQuantity();
        }
    }

    /**
     * Calculate the sub-total order by multiplying price by quantity
     *
     * @return the sub-total price
     */
    private float calculateSubtotal() {
        // Calculate the sub-total order by multiplying price by quantity
        subtotal = (productsQuantity * Float.parseFloat(price));
        double subtotalDouble = subtotal;
        return (float) decimalsRounder(subtotalDouble, 2);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity() {
        if (productsQuantity != 0) {
            edtQuantity.setText(Integer.toString(productsQuantity));
        } else {
            edtQuantity.setText("1");
        }
        tvPrice.setText("$" + price);
        tvQuantity.setText(Integer.toString(productsQuantity));
        tvSubtotal.setText("$" + calculateSubtotal());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_minus: {
                decrement();
                break;
            }
            case R.id.btn_plus: {
                increment();
                break;
            }
            case R.id.btn_cancel: {
                getDialog().cancel();
                break;
            }
            case R.id.btn_add: {
                addProductRequest();
                break;
            }
        }
    }

    private void addProductRequest() {
        ShoppingCartTokenModel shoppingCartTokenModel = new ShoppingCartTokenModel(token, tokenSecret, productsQuantity);

        String str = edtQuantity.getText().toString();
        if (str.matches("")) {
            Toast.makeText(getActivity(), R.string.field_can_not_be_empty, Toast.LENGTH_SHORT).show();
        } else if (!str.matches("")) {

            if (commingFrom.equals("isSearch")) {
                Toast.makeText(getActivity(), R.string.adding_to_cart, Toast.LENGTH_SHORT).show();
            } else if (commingFrom.equals("isChild")) {
                Toast.makeText(getActivity(), R.string.modifying_list, Toast.LENGTH_SHORT).show();
            }
            progressBar.setVisibility(View.VISIBLE);

            Call<EMesResponse> call = RestClient.buildService().sendShoppingCartRequest(productId, shoppingCartTokenModel);
            call.enqueue(new Callback<EMesResponse>() {
                @Override
                public void onResponse(Call<EMesResponse> call, Response<EMesResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getE() == 0) {
                            getDialog().cancel();
                            // Update profile data
                            if (commingFrom.equals("isSearch")) {
                                Toast.makeText(getActivity(), R.string.added_to_shoppingcart, Toast.LENGTH_LONG).show();
                            } else if (commingFrom.equals("isChild")) {
                                Toast.makeText(getActivity(), R.string.product_quantity_modified, Toast.LENGTH_LONG).show();
                                refreshChild();
                            }
                        } else {
                            String error = response.body().getMes();
                            getDialog().cancel();
                            if (commingFrom.equals("isSearch")) {
                                Toast.makeText(getActivity(), R.string.could_not_be_added, Toast.LENGTH_LONG).show();
                            } else if (commingFrom.equals("isChild")) {
                                Toast.makeText(getActivity(), R.string.could_not_be_modified, Toast.LENGTH_LONG).show();
                            }

                        }
                    } else {
                        String error = response.body().getMes();
                        getDialog().cancel();
                        if (commingFrom.equals("isSearch")) {
                            Toast.makeText(getActivity(), R.string.could_not_be_added, Toast.LENGTH_LONG).show();
                        } else if (commingFrom.equals("isChild")) {
                            Toast.makeText(getActivity(), R.string.could_not_be_modified, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EMesResponse> call, Throwable t) {
                    Log.i("retro_mes", t.toString());
                    getDialog().cancel();
                    String error = t.toString();
                    if (commingFrom.equals("isSearch")) {
                        Toast.makeText(getActivity(), R.string.could_not_be_added, Toast.LENGTH_LONG).show();
                    } else if (commingFrom.equals("isChild")) {
                        Toast.makeText(getActivity(), R.string.could_not_be_modified, Toast.LENGTH_LONG).show();
                    }
                }
            });
            return;

        }
    }

    public void inputQuantityManually() {
        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!s.toString().equals("")) {
                    productsQuantity = Integer.parseInt(s.toString());
                    if (productsQuantity >= 0) {
                        textChangeDisplayQuantity();
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    productsQuantity = Integer.parseInt(s.toString());
                    if (productsQuantity >= 0) {
                        textChangeDisplayQuantity();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void textChangeDisplayQuantity() {
        tvPrice.setText("$" + price);
        tvQuantity.setText("" + productsQuantity);
        tvSubtotal.setText("$" + calculateSubtotal());
    }

    private class InputFilterMinMax implements InputFilter {

        private int min, max;

        InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) {
                String message = nfe.getMessage();
            }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

    public void refreshChild() {
        Intent intent = new Intent("refresh");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
}