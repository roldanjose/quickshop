package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.pojos.FindIdTransitionResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by WAMS 5 on 17/03/2017.
 */

public class FragmentFindOrderId extends Fragment {

    private String token;
    private String tokensecret;

    private EditText edtOrderId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_find_product, container, false);
        initComponents(view);

        return view;
    }

    private void initComponents(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(getString(R.string.billing));
        LocalStorage localStorage = new LocalStorage(getContext());
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokensecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        Button btnRequest = view.findViewById(R.id.btn_request);
        edtOrderId = view.findViewById(R.id.edt_order_id);

        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findOrderByIdRequest();
            }
        });
    }

    private void findOrderByIdRequest() {
        String orderIdText = edtOrderId.getText().toString();

        Call<FindIdTransitionResponse> call = RestClient.buildService().
                findIdOrder(token, tokensecret, orderIdText);
        call.enqueue(new Callback<FindIdTransitionResponse>() {
            @Override
            public void onResponse(Call<FindIdTransitionResponse> call,
                                   Response<FindIdTransitionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        // Message Id Order Transiction
                        Toast.makeText(getActivity(), response.body().getMes(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FindIdTransitionResponse> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });

    }
}
