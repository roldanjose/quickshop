package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.ChangePasswordModel;
import com.wamsmobile.kepapaya.pojos.ChangePasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by WAMS 5 on 17/03/2017.
 */

public class FragmentPasswordChange extends Fragment {

    private EditText edtIdPassword, edtIdPasswordNew;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        initComponents(view);

        return view;
    }

    private void initComponents(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(getString(R.string.change_password));

        Button btnUpdatePassword = view.findViewById(R.id.btn_change_password);
        edtIdPassword = view.findViewById(R.id.edt_password);
        edtIdPasswordNew = view.findViewById(R.id.edt_change_new_password);

        btnUpdatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordRequest();
            }
        });
    }

    public void changePasswordRequest() {
        LocalStorage localStorage = new LocalStorage(getContext());
        String token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        String tokensecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        String password = edtIdPassword.getText().toString();
        String newPassword = edtIdPasswordNew.getText().toString();

        ChangePasswordModel changePasswordModel = new ChangePasswordModel(password, newPassword, token, tokensecret);

        Call<ChangePasswordResponse> call = RestClient.buildService().changePassword(changePasswordModel);
        call.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        // Toast change of password
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                    } else {
                        String error = response.body().getMes();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Log.i("retro_mes", t.toString());
                String error = t.toString();
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });
    }
}
