package com.wamsmobile.kepapaya.fragments.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;

/**
 * Created by dell on 30/05/2017.
 */

public class TermsConditionsFragment extends Fragment implements View.OnClickListener {

    private AuthenticationActivity activity;

    private ImageButton btnClose, btnContinue;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terms_conditions, container, false);
        initComponents(view);
        setOnClickListeners();
        return view;
    }

    private void initComponents(View view) {
        activity = (AuthenticationActivity) getActivity();

        btnClose = view.findViewById(R.id.btn_cancel);
        btnContinue = view.findViewById(R.id.btn_continue);
        progressBar = view.findViewById(R.id.progress_bar);

    }

    private void setOnClickListeners() {
        btnClose.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:

                break;
            case R.id.btn_continue:

                break;
        }
    }

}
