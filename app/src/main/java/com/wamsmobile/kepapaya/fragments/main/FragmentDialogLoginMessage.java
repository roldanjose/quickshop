package com.wamsmobile.kepapaya.fragments.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;

/**
 * Custom Dialog Fragment that prompts the user to login to continue
 */

public class FragmentDialogLoginMessage extends DialogFragment {

    public static final String ARG_ITEM_ID = "FragmentDialogLoginMessage";

    private ImageButton btnContinue;

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        initComponents(dialog);
        return dialog;
    }

    private void initComponents(Dialog dialog) {
        // Dialog configuration
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_VERTICAL);

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        window.setBackgroundDrawable(new ColorDrawable(0x7000000));
        layoutParams.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(layoutParams);

        // Set the layout for the dialog
        dialog.setContentView(R.layout.fragment_dialog_login_message);
        dialog.setCancelable(true);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Translucent;

        // Find views
        btnContinue = dialog.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
                startActivity(intent);
            }
        });
    }
}