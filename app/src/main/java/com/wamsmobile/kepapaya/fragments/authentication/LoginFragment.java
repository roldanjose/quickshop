package com.wamsmobile.kepapaya.fragments.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AuthenticationActivity;
import com.wamsmobile.kepapaya.application.KepapayaApplication;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.models.Login;
import com.wamsmobile.kepapaya.pojos.EMesResponse;
import com.wamsmobile.kepapaya.pojos.Email;
import com.wamsmobile.kepapaya.pojos.LoginResponse;
import com.wamsmobile.kepapaya.utilities.EditTextView;
import com.wamsmobile.kepapaya.utilities.Label;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 30/05/2017.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {

    private ProgressBar progressBar;
    boolean isEmptyEmail, isEmptyPassword;
    private EditTextView edtEmail, edtPassword;
    private TextInputLayout tilEmail, tilPassword;
    private ImageButton btnLogin;
    private Label tvForgotPassword, tvRegister;
    private AuthenticationActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initComponents(view);
        setOnClickListeners();
        return view;
    }

    private void initComponents(View view) {
        activity = (AuthenticationActivity) getActivity();

        edtEmail = view.findViewById(R.id.edt_email);
        edtPassword = view.findViewById(R.id.edt_password);
        tilEmail = view.findViewById(R.id.email_input_layout);
        tilPassword = view.findViewById(R.id.password_input_layout);
        btnLogin = view.findViewById(R.id.btn_login);
        tvForgotPassword = view.findViewById(R.id.tv_forgot_password);
        tvRegister = view.findViewById(R.id.tv_register);
        progressBar = view.findViewById(R.id.progress_bar);

        isEmptyEmail = activity.isEmpty(edtEmail);
        isEmptyPassword = activity.isEmpty(edtPassword);
    }

    private void setOnClickListeners() {
        tvForgotPassword.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login: {
                loginRequest();
                break;
            }
            case R.id.tv_forgot_password: {
                recoverPasswordRequest();
                break;
            }
            case R.id.tv_register: {
                activity.initFragment(new RegisterFragment());
                break;
            }
        }
    }

    private void recoverPasswordRequest() {
        if (isEmptyEmail) {
            tilEmail.setError(getString(R.string.required_field));
        } else {
            String email = edtEmail.getText().toString().trim();
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                tilEmail.setError(getString(R.string.invalid_email));
            } else {
                progressBar.setVisibility(View.VISIBLE);

                Email userEmail = new Email(email);

                Call<EMesResponse> call = RestClient.buildService().
                        forgotPassword(userEmail);
                call.enqueue(new Callback<EMesResponse>() {
                    @Override
                    public void onResponse(Call<EMesResponse> call,
                                           Response<EMesResponse> response) {
                        if (response.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            if (response.body().getE() == 0) {
                                Toast.makeText(getActivity(),
                                        R.string.email_has_been_sent, Toast.LENGTH_LONG).show();
                                activity.initFragment(new ForgotFragment());
                            } else {
                                progressBar.setVisibility(View.GONE);
                                String error = response.body().getMes();
                                Toast.makeText(getActivity(),
                                        error, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            String error = response.errorBody().toString();
                            Toast.makeText(getActivity(),
                                    error, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<EMesResponse> call, Throwable t) {
                        Log.i("retro_mes", t.toString());
                        progressBar.setVisibility(View.GONE);
                        String error = t.toString();
                        Toast.makeText(getActivity(),
                                error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    private void loginRequest() {
        if (isEmptyEmail) {
            tilEmail.setError(getString(R.string.required_field));
        } else if (isEmptyPassword) {
            tilPassword.setError(getString(R.string.required_field));
        } else {
            String mail = edtEmail.getText().toString().trim();
            if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                tilEmail.setError(getString(R.string.invalid_email));
            } else {
                String userEmail = edtEmail.getText().toString().trim();
                String userPassword = edtPassword.getText().toString().trim();
                progressBar.setVisibility(View.VISIBLE);

                Login userLogin = new Login(userEmail, userPassword);

                Call<LoginResponse> call = RestClient.buildService().loginUser(userLogin);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call,
                                           Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            if (response.body().getE() == 0) {
                                LocalStorage localStorage = new
                                        LocalStorage(getActivity());
                                localStorage.setPermanentVariable(KeyStore.TOKEN,
                                        response.body().getToken());
                                localStorage.setPermanentVariable(KeyStore.TOKEN_SECRET,
                                        response.body().getTokenSecret());
                                KepapayaApplication.showToast(activity, R.string.logged_in, Toast.LENGTH_SHORT);
                                activity.finish();
                            } else {
                                progressBar.setVisibility(View.GONE);
                                String error = response.body().getMes();
                                Toast.makeText(getActivity(),
                                        error, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            String error = response.errorBody().toString();
                            Toast.makeText(getActivity(),
                                    error, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.i("retro_mes", t.toString());
                        progressBar.setVisibility(View.GONE);
                        String error = t.toString();
                        Toast.makeText(getActivity(),
                                error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }
}
