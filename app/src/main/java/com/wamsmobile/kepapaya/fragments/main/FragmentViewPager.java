package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.adapters.ShoppingPayAdapter;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.LocalStorage;
import com.wamsmobile.kepapaya.libs.RestClient;
import com.wamsmobile.kepapaya.pojos.Dispatch;
import com.wamsmobile.kepapaya.pojos.DispatchResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Fragment
 * Created by dell on 08/05/2017.
 */

public class FragmentViewPager extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private List<Dispatch> dispatches;
    private ShoppingPayAdapter adapter;
    private String token, tokensecret;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private int max, start, status;
    private boolean onRefreshed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shopping_pay, container, false);
        initComponents(view);
        configRecyclerView();

        load(max, start, status, onRefreshed);
        return view;
    }

    private void initComponents(View view) {
        max = getArguments().getInt("max");
        start = getArguments().getInt("start");
        status = getArguments().getInt("status");
        LocalStorage localStorage = new LocalStorage(getContext());
        token = localStorage.getPermanentVariable(KeyStore.TOKEN);
        tokensecret = localStorage.getPermanentVariable(KeyStore.TOKEN_SECRET);

        mSwipeRefreshLayout = view.findViewById(R.id.swiperefresh_shopping);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = view.findViewById(R.id.recycler_shopping_pay);
        dispatches = new ArrayList<>();
    }

    private void configRecyclerView() {
        adapter = new ShoppingPayAdapter(getContext(), dispatches);
        adapter.setLoadMoreListener(new ShoppingPayAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        start = dispatches.size();
                        // a method which requests remote data
                        loadMore(token, tokensecret, max, start, status);
                    }
                });
                //Calling loadMore function in Runnable to fix the
                // java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing a layout or scrolling error
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void load(int max, int start, int status, final boolean onRefreshed) {
        mSwipeRefreshLayout.setRefreshing(true);

        Call<DispatchResponse> call = RestClient.buildService().
                getDispatches(token, tokensecret, max, start, status);
        call.enqueue(new Callback<DispatchResponse>() {
            @Override
            public void onResponse(Call<DispatchResponse> call,
                                   Response<DispatchResponse> response) {
                if (response.isSuccessful()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (response.body().getE() == 0) {
                        if (onRefreshed) {
                            dispatches = new ArrayList<>();
                            dispatches.addAll(response.body().getDispatches());
                            configRecyclerView();
                        } else {
                            dispatches.addAll(response.body().getDispatches());
                            adapter.notifyDataChanged();
                        }
                    } else {
                        mSwipeRefreshLayout.setRefreshing(false);
                        String error = response.body().getMes();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.e(TAG, " Response Error " + String.valueOf(response.code()));
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DispatchResponse> call, Throwable t) {
                Log.e(TAG, " Response Error " + t.getMessage());
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    mSwipeRefreshLayout.setRefreshing(false);
                    String error = t.toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void loadMore(String token, String tokensecret, int max, int start, int status) {
        //add loading progress view
        dispatches.add(new Dispatch("load"));
        adapter.notifyItemInserted(dispatches.size() - 1);

        Call<DispatchResponse> call = RestClient.buildService().
                getDispatches(token, tokensecret, max, start, status);
        call.enqueue(new Callback<DispatchResponse>() {
            @Override
            public void onResponse(Call<DispatchResponse> call,
                                   Response<DispatchResponse> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    dispatches.remove(dispatches.size() - 1);

                    List<Dispatch> result = response.body().getDispatches();
                    if (result.size() >= 1) {
                        //add loaded data
                        dispatches.addAll(result);
                    } else if (result.size() == 0) {
                        //result size 0 means there is no more data available at server
                        adapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(getContext(), "no more dispaches available", Toast.LENGTH_LONG).show();
                    }
                    adapter.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<DispatchResponse> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    // Method to View Pager
    public static FragmentViewPager newInstance(int max, int start, int status) {
        Bundle args = new Bundle();
        FragmentViewPager fragment = new FragmentViewPager();
        args.putInt("max", max);
        args.putInt("start", start);
        args.putInt("status", status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onRefresh() {
        load(5, 0, status, true);
    }
}