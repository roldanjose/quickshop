package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.wamsmobile.kepapaya.application.KepapayaApplication.decimalsRounder;

/**
 * Created by dell on 30/05/2017.
 */

public class FragmentMyShoppingDetails extends Fragment implements OnMapReadyCallback {

    private TextView cartTransactionId, carDatePay, carDateDelay,
            carCharged, dispatchText, dispatchEmail, dispatchPhone;

    private Button btnBackShoppingDetail;
//
//    @BindView(R.id.dispatch_img)
//    TextView dispatch_img;

    // TODO: 30/03/2017 SwipeRefresh
//    @BindView(R.id.swiperefresh_shopping_detail)
//    SwipeRefreshLayout mSwipeRefreshLayout;

    private String bundleIdTransactionPass, bundleColony;
    private long bundleCartDatePay, bundleCartDateDelivery;
    private Double bundleCartCharged;
    private Double bundleCoordinateX, bundleCoordinateY;

    // Google Maps
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_my_shopping_details, container, false);

        // mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh_shopping);
        initComponents(view);
        extraCarShoppingPay();
        //Bundle extra = getIntent().getExtras();

        cartTransactionId.setText(bundleIdTransactionPass);
        carDatePay.setText(getFormattedDateFromTimestamp(bundleCartDatePay));
        carDateDelay.setText(getFormattedDateFromTimestamp(bundleCartDateDelivery));
        String cartCharged = Double.toString(bundleCartCharged);
        carCharged.setText(cartCharged);

        // Dispatch
        dispatchText.setText(bundleColony);
        // dispatchPhone.setText(bundle_phone);
        // dispatchEmail.setText(bundle_email);

        return view;
    }

    private void initComponents(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(getString(R.string.my_shopping_details));

        // Cart view
        cartTransactionId = view.findViewById(R.id.txt_cart_transactation_id);
        carDatePay = view.findViewById(R.id.txt_cart_date_pay);
        carDateDelay = view.findViewById(R.id.txt_cart_date_delay);
        carCharged = view.findViewById(R.id.txt_car_charged);

        // Dispatch views
        dispatchText = view.findViewById(R.id.txt_dispatch_test);
        dispatchEmail = view.findViewById(R.id.txt_dispatch_email);
        dispatchPhone = view.findViewById(R.id.txt_dispatch_phone);

        // Button Back
        btnBackShoppingDetail = view.findViewById(R.id.btn_back);
        btnBackShoppingDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().
                        remove(FragmentMyShoppingDetails.this).commit();
            }
        });

        mMapView = view.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    // Method to convert to a date format
    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds) {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

    public void extraCarShoppingPay() {

//        mSwipeRefreshLayout.setRefreshing(true);
        Bundle extra = getActivity().getIntent().getExtras();

        bundleIdTransactionPass = extra.getString("idTransactionPass");
        bundleCartDatePay = (long) extra.getFloat("cartDatePay");
        bundleCartDateDelivery = (long) extra.getFloat("cartDateDelivery");
        bundleCartCharged = decimalsRounder(extra.getDouble("cartCharged"), 2);

        // Dispatch by
        bundleColony = extra.getString("dispatchTest");
        // bundle_phone = extra.getString("dispatchPhone");
        // bundle_email = extra.getString("dispatchEmail");

        // Maps
        bundleCoordinateX = extra.getDouble("mapCoordinateX");
        bundleCoordinateY = extra.getDouble("mapCoordinateY");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());

        mGoogleMap = googleMap;
        extraCarShoppingPay();

        LatLng latLng = new LatLng(bundleCoordinateY, bundleCoordinateX);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        CameraPosition cameraPosition = CameraPosition.builder().target
                (new LatLng(bundleCoordinateY, bundleCoordinateX)).zoom(15).bearing(5).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
