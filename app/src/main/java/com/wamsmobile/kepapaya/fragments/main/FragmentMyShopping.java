package com.wamsmobile.kepapaya.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.adapters.MyShoppingPagerAdapter;

/**
 * Created by WAMS 5 on 20/03/2017.
 */

public class FragmentMyShopping extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_shopping, container, false);
        initComponents(view);
        return view;
    }

    private void initComponents(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(getString(R.string.my_shopping));

        // Find views
        ViewPager viewPager = view.findViewById(R.id.viewpager);
        TabLayout tabLayout = view.findViewById(R.id.appbartabs_shopping);

        // Associate adapter to the viewpager
        viewPager.setAdapter(new MyShoppingPagerAdapter(getFragmentManager(), getActivity()));

        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }
}
