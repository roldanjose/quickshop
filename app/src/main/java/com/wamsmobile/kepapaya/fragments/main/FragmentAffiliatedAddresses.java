package com.wamsmobile.kepapaya.fragments.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wamsmobile.kepapaya.R;
import com.wamsmobile.kepapaya.activities.AddAddressActivity;
import com.wamsmobile.kepapaya.activities.MainActivity;
import com.wamsmobile.kepapaya.adapters.AffiliatedAdressesAdapter;
import com.wamsmobile.kepapaya.adapters.CollectionAdapter;
import com.wamsmobile.kepapaya.holders.AffiliatedAddressesHolder;
import com.wamsmobile.kepapaya.interfaces.KeyStore;
import com.wamsmobile.kepapaya.libs.RestClient360;
import com.wamsmobile.kepapaya.models.AddressesModel;
import com.wamsmobile.kepapaya.pojos.Address;
import com.wamsmobile.kepapaya.utilities.ButtonLabel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wamsmobile.kepapaya.interfaces.KeyStore.FROM_BUTTON;

/**
 * Created by dell on 10/04/2017.
 */

public class FragmentAffiliatedAddresses extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private MainActivity activity;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private ButtonLabel btnAddAddress;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_affiliated_addresses, container, false);
        initComponents(view);
        setListeners();
        return view;
    }

    private void initComponents(View view) {
        activity = (MainActivity) getActivity();
        activity.setToolbarTitle(getString(R.string.affiliated_addresses));

        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefresh = view.findViewById(R.id.swiperefresh);
        btnAddAddress = view.findViewById(R.id.btn_add_address);

        LocalBroadcastManager.getInstance(activity).registerReceiver(mMessageReceiver, new IntentFilter(KeyStore.REFRESH_ADDRESSES));
        addressesListRequest();
    }

    private void setListeners(){
        swipeRefresh.setOnRefreshListener(this);
        btnAddAddress.setOnClickListener(this);
    }

    public void addressesListRequest() {
        swipeRefresh.setRefreshing(true);
        // TODO:
        String token = "vkl1DQW84";
        String tokenSecret = "579e4e3d-070c-49f5-972d-493ce814c96b";

        Call<AddressesModel> call = RestClient360.buildService().getAddressesList360(token, tokenSecret);
        call.enqueue(new Callback<AddressesModel>() {
            @Override
            public void onResponse(Call<AddressesModel> call, Response<AddressesModel> response) {

                if (response.isSuccessful()) {
                    if (response.body().getE() == 0) {
                        swipeRefresh.setRefreshing(false);
                        List<Address> adresses = response.body().getAddresses();
                        AffiliatedAdressesAdapter mAdapter = new AffiliatedAdressesAdapter
                                (getContext(), adresses, R.layout.item_affiliated_addresses);
                        mAdapter.setOnItemLongClickListener(new CollectionAdapter.OnItemLongClickListener<Address, AffiliatedAddressesHolder>() {
                            @Override
                            public void onItemLongClick(Address item, AffiliatedAddressesHolder holder, int position) {
                                startAddAddressActivity(item);
                            }
                        });
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(mAdapter);
                    } else {
                        swipeRefresh.setRefreshing(false);
                        String error = response.errorBody().toString();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    swipeRefresh.setRefreshing(false);
                    String error = response.errorBody().toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AddressesModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    Log.i("retro_mes", t.toString());
                    String error = t.toString();
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        addressesListRequest();
    }

    private void startAddAddressActivity(Address item) {
        Intent intent = new Intent(activity, AddAddressActivity.class);
        intent.putExtra("ADDRESSID", item.getId());
        intent.putExtra("ALIAS", item.getAlias());
        intent.putExtra("ADDRESS", item.getAddress());
        intent.putExtra("CONTACT", item.getContact());
        intent.putExtra("EXTERIORINFO", item.getNumext());
        intent.putExtra("INTERIORINFO", item.getNumint());
        intent.putExtra("POSTALCODE", item.getPostal());
        intent.putExtra("COLONY", item.getColony());
        intent.putExtra("STATE", item.getState());
        intent.putExtra("CITY", item.getCity());
        intent.putExtra("LATITUDE", Double.toString(item
                .getPosition().getCoordinates().get(1)));
        intent.putExtra("LONGITUDE", Double.toString(item
                .getPosition().getCoordinates().get(0)));
        intent.putExtra(FROM_BUTTON, KeyStore.EDIT_MENU);

        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        activity.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_address: {
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                Intent intent = new Intent(getActivity(), AddAddressActivity.class);
                intent.putExtra(FROM_BUTTON, KeyStore.ADD_ADDRESS);
                startActivity(intent);
                break;
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            addressesListRequest();
        }
    };

    @Override
    public void onDestroy() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(
                mMessageReceiver);
        super.onDestroy();
    }

}
